# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

import logging

from lxml import etree

from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

from urllib.parse import urlencode
from urllib.request import Request, urlopen
import ssl
import requests
import json
ssl._create_default_https_context = ssl._create_unverified_context


class DeskripsiSubKriteria(models.Model):
    _name = 'deskripsi.sub.kriteria'
    _description = 'Deskripsi Sub Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Text('Name')
    kode = fields.Text('Kode')
    point = fields.Float('Point')
    sub_kriteria_id = fields.Many2one('sub.kriteria', string='Sub Kriteria')


class SubKriteria(models.Model):
    _name = 'sub.kriteria'
    _description = 'Sub Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Char('Name')
    kriteria_id = fields.Many2one('kriteria.kriteria', string='Kriteria')
    ket = fields.Text('Keterangan')


class KriteriaKriteria(models.Model):
    _name = 'kriteria.kriteria'
    _description = 'Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Char('Name')


class PenilaianPenilaina(models.Model):
    _name = 'penilaian.penilaian'
    _description = 'Penilaian'

    kriteria_id = fields.Many2one('kriteria.kriteria', string='Kriteria')
    sub_kriteria_id = fields.Many2one('sub.kriteria', string='Sub Kriteria')
    des_sub_kriteria_id = fields.Many2one('deskripsi.sub.kriteria', string='Deskripsi Sub-Kriteria')
    kode = fields.Char('Kode')
    point = fields.Float('point')
    ket = fields.Text('Keterangan')

    penilaian_user_id = fields.Many2one('penilaian.user', string='Penilaian User', ondelete="cascade")

    @api.onchange('sub_kriteria_id')
    def _onchange_sub_kriteria_id(self):
        if self.sub_kriteria_id:
            self.kriteria_id = self.sub_kriteria_id.kriteria_id.id
            self.ket = self.sub_kriteria_id.ket
            # return {
            #     'domain':{
            #         'des_sub_kriteria_id': [('sub_kriteria_id','=', self.sub_kriteria_id.id)],
            #     }
            # }

    @api.onchange('des_sub_kriteria_id')
    def _onchange_des_sub_kriteria_id(self):
        if self.des_sub_kriteria_id:
            self.sub_kriteria_id = self.des_sub_kriteria_id.sub_kriteria_id.id
            self.kode = self.des_sub_kriteria_id.kode
            self.point = self.des_sub_kriteria_id.point
            self.ket = self.des_sub_kriteria_id.sub_kriteria_id.ket


class PenilaianUser(models.Model):
    _name = 'penilaian.user'
    _description = 'Penilaian User'

    def juri(self):
        group_obj = self.env['res.groups'].search([('name', 'in', ['ss Manager', 'ss Admin Komite'])])
        if group_obj:
            return [('id', 'in', group_obj.users.ids)]

    @api.model
    def default_get(self, field_list):
        res = super(PenilaianUser, self).default_get(field_list)
        group_obj = self.env['res.groups'].search([('name', 'in', ['ss Manager', 'ss Admin Komite'])])
        if group_obj:
            if self.env.uid in group_obj.users.ids:
                res_data = []
                sub_ktriteria = self.env['sub.kriteria'].search([])
                if sub_ktriteria:
                    for rec in sub_ktriteria:
                        sub_data = {
                            'kriteria_id': rec.kriteria_id.id,
                            'sub_kriteria_id': rec.id,
                            'ket': rec.ket,
                        }
                        res_data.append((0, 0, sub_data))
                res.update({
                    'user_id': self.env.uid,
                    'penilaian_ids': res_data,
                })
        return res

    user_id = fields.Many2one('res.users', string='User', domain=juri)
    total = fields.Float('Total', compute='_compute_total_user', store=True)
    penilaian_ids = fields.One2many('penilaian.penilaian', 'penilaian_user_id', string='Penilaian Kriteria',)

    sugestion_id = fields.Many2one('suggestion.system', string='Suggestion System', ondelete="cascade")

    @api.depends('penilaian_ids')
    def _compute_total_user(self):
        for rec in self:
            if len(rec.penilaian_ids) > 0:
                rec.total = sum([x.point for x in rec.penilaian_ids if rec.penilaian_ids])
            else:
                rec.total = 0


class SuggestionSystem(models.Model):
    _inherit = 'suggestion.system'
    _description = 'Suggestion System'

    penilaian_kriteria_ids = fields.One2many('penilaian.user', 'sugestion_id', string='Penilaian Kriteria')
    dampak = fields.Selection([('safety', 'Safety / 5R'), ('cost', 'Cost'), ('productivity', 'Productivity'), ('environment', 'Environment'),
                              ('quality', 'Quality'), ('delivery', 'Delivery'), ('morale', 'Morale'), ('others', 'Others')], string='Dampak', required=True)
    level = fields.Selection([
        ('bronze', 'Bronze'),
        ('silver', 'Silver'),
        ('gold', 'Gold'),
        ('platinum', 'Platinum'),
        ('diamond', 'Diamond')
    ], string='Level', compute='_compute_level', store=True)
    avarage = fields.Float('Avarage', compute='_compute_level', store=True)

    @api.depends('penilaian_kriteria_ids')
    def _compute_level(self):
        for rec in self:
            jumlah = 0
            if len(rec.penilaian_kriteria_ids) > 0:
                jumlah = sum([x.total for x in rec.penilaian_kriteria_ids if rec.penilaian_kriteria_ids]) / len(rec.penilaian_kriteria_ids)
                rec.avarage = float(jumlah)

                if jumlah <= 50:
                    rec.level = 'bronze'
                elif jumlah > 50.1 and jumlah < 65:
                    rec.level = 'silver'
                elif jumlah >= 65.1 and jumlah < 82:
                    rec.level = 'gold'
                elif jumlah >= 82.1 and jumlah < 93:
                    rec.level = 'diamond'
                elif jumlah >= 93.1:
                    rec.level = 'diamond'

            else:
                rec.level = False
                rec.avarage = float(jumlah)

    def set_to_done_ss_admin_komite(self):
        for rec in self:
            rec.write({'state': 'done'})
        
    def kirim_data_ss(self):
        for rec in self:
            perbaikan = []
            for data in rec.perbaikan_lines:
                perbaikan.append(data.perbaikan)
            
            kategory = ''
            if rec.dampak in ['cost', 'delivery']:
                kategory = 'Cost Reduction-Suggestion System'
            elif rec.dampak == 'productivity':
                kategory = 'Productivity & Production System-Suggestion System'
            elif rec.dampak in ['safety', 'morale', 'environment', 'others']:
                kategory = 'Safety, 5R & Environment-Safety, 5R & Environment'
            elif rec.dampak == 'quality':
                kategory = 'Quality-Suggestion System'

            url = 'https://enlightlibrary.triputra-group.com/api/api_enlight_send_other.php?action=uploadSS' # Set destination URL here
            post_fields = {
                'FS_NIK': "1308131",
                # 'FS_NIK': str(rec.name),
                'FS_SUBCO':'Daya Adicipta Sandika',
                'FS_CAT': kategory,
                'FS_SBJ':'TEMA SS %s' % (rec.tgl_ide),
                'FS_ABSTRAK':str(perbaikan),
                'FS_NAMATIM':'',
                'FS_ANGGOTA': str(rec.employee_id.name),
                'key_API':'TR1pUTR@65844438rugbfsbjfjgdgfdjGGr',
                'file_contents':''
            }     # Set POST fields here
            
            request = Request(url, urlencode(post_fields).encode())
            result = urlopen(request).read().decode()
            res = json.loads(result)
            if res['upload_theme']:
                for data in res['upload_theme']:
                    if data['STATUS'] != 1 or data['STATUS'] != '1':
                        raise ValidationError(_(data['MSG']))
