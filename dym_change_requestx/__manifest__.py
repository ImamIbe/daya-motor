{
    "name":"DYM Change Request",
    "version":"1.0",
    "author":"Fikra",
    "category":"Custom Module",
    "description": """
        Change Request Form
    """,
    "depends":[
        "base",
        "mail",
        "hr",
        # "dym_dealer_menu",
        # "dym_approval",
    ],
    "data":[            
        "views/dym_change_request_view.xml", 
        "report/dym_change_request_template.xml",
        "views/change_request_form.xml",
        "views/dym_change_request_dev_view.xml",
        "views/dym_change_request_uat_view.xml",
        "security/groups.xml",
        "security/ir_rule.xml",
        "security/ir.model.access.csv",
        "data/dym_change_request_data.xml",
    ],
    "active":False,
    "installable":True,
    # "css":['static/src/css/c']
}