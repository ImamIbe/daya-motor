import time
from datetime import datetime
from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError
from openerp import SUPERUSER_ID
import logging
_logger = logging.getLogger(__name__)
class dym_change_req(models.Model):
    _name = 'dym.change.request'
    _description = "Change Request Form"
    _order = "sequence,id"

    # @api.one
    @api.depends('work_area')
    def _get_division(self):
        # print("self.work_area", self.work_area)
        # if self.work_area:
        for x in self:
            if x.work_area:
                if x.work_area == "Unit":
                    self.division = 'Unit'
                elif x.work_area == "Sparepart":
                    self.division = 'Sparepart'
                elif x.work_area == "Umum":
                    self.division = 'Umum'
                elif x.work_area in ("Finance","Accounting"):
                    self.division = 'Finance'
            else:
                self.division = 'Umum'

    def _get_it_head(self):
        it_head = self.env['hr.department'].search([('name','=','INFORMATION & TECHNOLOGY - IT')])
        if it_head:
            return it_head[0].manager_id.id
    
    def _get_it_analyst(self):
        it_analyst = self.env['hr.employee'].search([('id','=',12608)])
        if it_analyst:
            return it_analyst[0].id
    
    def _get_it_section(self):
        # 8 = Asep Jamaludin
        it_section = self.env['hr.employee'].search([('id','=',8)])
        if it_section:
            return it_section[0].id

    def _get_manager(self):
        user = self.env['hr.employee'].search([('user_id','=',self._uid)])
        manager_src = self.env['hr.employee'].search([('id','=',user.department_id.manager_id.id)])
        return manager_src.id

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('waiting_for_approval','Waiting Approval'),
        ('approved_manager', 'P/E.Manager Approved'),
        ('approved_it_analyst','IT Analyst Approved'),
        ('approved_it_section','IT Section Head Approved'),
        ('approved', 'IT Head Approved'),
        ('cancel', 'Cancelled'),
        ('refuse', 'Refused'),
        ('done', 'Done'),
    ]

    STATE_PROCESS_SELECTION = [
        ('dev', 'Development'),
        ('uat','UAT'),
        ('wfa_it_analyst','Waiting Approval IT Analyst'),
        ('to_production','To Production'),
        ('finish','Finish')
    ]

    STATE_DEV_SELECTION = [
        ('in_progress','In Progress'),
        ('break','Break'),
        ('finish','Finish'),
    ]

    STATE_UAT_SELECTION = [
        ('in_progress','In Progress'),
        ('break','Break'),
        ('finish','Finish'),
    ]

    @api.depends('manager')
    def _get_department(self):
        for cr in self:
            if cr.manager:
                employee = self.env['hr.employee'].search([('user_id','=',self._uid)])
                cr.department = employee.department_id.id
            else:
                cr.department = False
    # def _get_sequence(self):
    #     for x in self:
    #         x.sequence = x.id

    @api.depends('developer_dev_ids')
    def _get_dev_ids(self):
        for x in self:
            if x.developer_dev_ids:
                dev=''
                if x.developer_dev_ids:
                    for y in x.developer_dev_ids:
                        dev += y.developer_id.name + '; '
                    x.list_developer_ids = dev
                else:
                    x.list_developer_ids = False
            else:
                x.list_developer_ids = False



    names = fields.Selection([('Project','Project'),('Enhancement','Enhancement')], 'Type', size=100, required=True)
    division = fields.Selection([('Unit','Showroom'),('Sparepart','Workshop'),('Umum','General'),('Finance','Finance'),('Accounting','Accounting')], 'Division', compute='_get_division', readonly=True)
    department = fields.Many2one('hr.department', 'Department', compute='_get_department', store=True)
    # employee = fields.Many2one('hr.employee','Employee',compute=get_employee)
    name = fields.Char('CR Name')
    pe_name = fields.Char('P/E. Name', size=100, required=True)
    ref_number = fields.Char('P/E. Ref. Number', size=100)
    sponsor = fields.Char('P/E. Sponsor', size=100)
    manager = fields.Many2one('hr.employee', 'P/E. Manager', required=True, default=_get_manager)
    it_head = fields.Many2one('hr.employee', 'IT Head', readonly=False, required=True, default=_get_it_head)
    # it_head = fields.Many2one('res.users', 'IT Head', readonly=False, required=True, domain=[('id','=',269)])
    it_analyst = fields.Many2one('hr.employee', string='IT Analyst', required=True, default=_get_it_analyst)
    it_section_head = fields.Many2one('hr.employee', string='IT Section Head', required=True, default=_get_it_section)
    bunit_head = fields.Many2one('res.users', 'Bussiness Unit Head', size=100)
    ithead_apl = fields.Many2one('res.users', 'IT Head Application', size=100)
    apl_own = fields.Many2one('res.users', 'Application Owner', size=100)
    work_area = fields.Selection([
                                ('Unit','Showroom'),
                                ('Sparepart','Workshop'),
                                ('Umum','General'),
                                ('Finance','Finance'),
                                ('Accounting','Accounting')],'Work Area', required=True)
    # no_change_req = fields.Char('No. Change Request', size=100)
    priority = fields.Selection([('High','High'),('Medium','Medium'),('Low','Low')], string='Priority', required=True)
    change_type = fields.Selection([('Fatal','Fatal'),('Major','Major'),('Minor','Minor')], string='Change Type', required=True)
    ori_end_date = fields.Date('Original End Date')
    proposed_end_date = fields.Date('Proposed End Date')
    curcon = fields.Text('Current Condition', size=100)
    curcon_doc = fields.Binary('Current Condition Doc')
    curcon_doc_name = fields.Text('Current Condition Doc Name')
    prochan = fields.Text('Proposed Change', size=100)
    prochan_doc = fields.Binary('Proposed Change Doc')
    prochan_doc_name = fields.Text('Proposed Change Doc Name')
    justi = fields.Text('Justification', size=100)
    justi_doc = fields.Binary('Justification Doc')
    justi_doc_name = fields.Text('Justification Doc Name')
    impass = fields.Text('Impact Assessment', size=100)
    functional = fields.Text('Functional', size=100)
    schedule = fields.Text('Schedule', size=100)
    effort = fields.Text('Effort', size=100)
    cost = fields.Text('Cost', size=100)
    exceptionform = fields.Text('Exception Form', size=100)
    approval_state =  fields.Selection([
                                        ('b','Belum Request'),
                                        ('rf','Request For Approval'),
                                        ('am','Approved P/E. Manager'),
                                        ('a','Approved'),
                                        ('r','Reject')
                                        ],'Approval State', readonly=True,default='b')
    state= fields.Selection(STATE_SELECTION, string='State', readonly=True,default='draft')
    # Tambahan Egi
    state_process = fields.Selection(STATE_PROCESS_SELECTION, readonly=True)
    state_dev = fields.Selection(STATE_DEV_SELECTION, readonly=True)
    state_uat = fields.Selection(STATE_UAT_SELECTION, readonly=True)
    # ----------- #
    print_report = fields.Text('Print', size=100)
    change_request_lines = fields.One2many('dym.change.request.line','cr_id',string="Approval")
    approval_ids = fields.One2many('dym.change.request.line', 'transaction_id', string="Table Approval")
    amount_total = fields.Integer('Amount Total')
    # Tambahan Egi
    start_dev = fields.Datetime('Start Develop',readonly=True)
    finish_dev = fields.Datetime('Finish Develop',readonly=True)
    date_break_dev = fields.Datetime('Break Develop',readonly=True)
    end_break_dev = fields.Datetime('End Break Develop',readonly=True)
    start_uat = fields.Datetime('Start UAT',readonly=True)
    finish_uat = fields.Datetime('Finish UAT',readonly=True)
    date_break_uat = fields.Datetime('Break UAT',readonly=True)
    end_break_uat = fields.Datetime('End Break UAT',readonly=True)
    developer_ids = fields.One2many('dym.change.request.developer.line','cr_id',string='Developer')
    user_ids = fields.One2many('dym.change.request.user.line', 'cr_id', string="User")
    change_request_dev_lines = fields.One2many('dym.change.request.dev', 'change_request_id', string="Change Request Dev")
    developer_dev_ids = fields.One2many('dym.change.request.dev.developer.line', related="change_request_dev_lines.developer_ids", string="Developer")
    change_request_dev_time_lines = fields.One2many('dym.change.request.dev.time.line',related="change_request_dev_lines.change_request_dev_time_lines",string="Development Timeline")
    change_request_uat_lines = fields.One2many('dym.change.request.uat', 'change_request_id', string="Change Request UAT")
    developer_uat_ids = fields.One2many('dym.change.request.uat.developer.line', related="change_request_uat_lines.developer_ids", string="Developer")
    change_request_uat_time_lines = fields.One2many('dym.change.request.uat.time.line',related="change_request_uat_lines.change_request_uat_time_lines",string="UAT Timeline")
    change_request_uat_notes = fields.One2many('dym.change.request.uat.note',related="change_request_uat_lines.change_request_uat_notes",string="Note Perbaikan")
    user_uat_ids = fields.One2many('dym.change.request.uat.user.line', related="change_request_uat_lines.user_ids", string="User")
    # ----------- #
    sequence = fields.Integer('Sequence',default=10)
    list_developer_ids = fields.Char('List Dev',compute=_get_dev_ids,store=True)


    # syahril 11-10-21
    estimasi_selesai_uat = fields.Date(string='Estimasi Selesai UAT')
    rencana_to_prod = fields.Date(string='Rencana to Production')

    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].get_sequence(self._cr, self._uid, 'IT.CRF')
        crfs = super(dym_change_req,self).create(values)

        return crfs

    # @api.multi
    def wkf_request_approval(self):
        self.write({'state':'waiting_for_approval', 'approval_state':'rf'})
        self.send_mail_change_req_approval()
        return True
    
    # @api.multi
    def wkf_approval(self):
        user_groups = self.env['res.users'].browse(self._uid)['groups_id']
        # raise Warning(("User group"+str(user_groups)))
        
        approval_line_ids = []
        approval_line_ids.append([0,0,{
                        'sts':'2',
                        'pelaksana_id':self._uid,
                        'tanggal':datetime.today(),
                        'division':self.division,
                    }])

        self.write({'approval_state':'am', 'state':'approved_manager', 'approval_ids':approval_line_ids})
        self.send_mail_change_req_approval_it(recipent=self.it_analyst.work_email)
        return True
    

    # @api.multi
    def wkf_approval_it_analyst(self):
        # VALIDASI
        if self.it_analyst.id != self.env.user.employee_id.id and self.env.user.id != 2:
            raise Warning("This Approval Belongs to {name}".format(
                name=self.it_analyst.name
            ))

        user_groups = self.env['res.users'].browse(self._uid)['groups_id']
        # raise Warning(("User group"+str(user_groups)))
                
        approval_line_ids = []
        approval_line_ids.append([0,0,{
                        'sts':'2',
                        'pelaksana_id':self._uid,
                        'tanggal':datetime.today(),
                        'division':self.division,
                    }])

        self.write({'approval_state':'am', 'state':'approved_it_analyst', 'approval_ids':approval_line_ids})
        self.send_mail_change_req_approval_it(recipent=self.it_section_head.work_email)
        return True
    
    def wkf_approval_it_section(self):
        # VALIDASI
        if self.it_section_head.id != self.env.user.employee_id.id and self.env.user.id != 2:
            raise Warning("This Approval Belongs to {name}".format(
                name=self.it_section_head.name
            ))
        user_groups = self.env['res.users'].browse(self._uid)['groups_id']
        # raise Warning(("User group"+str(user_groups)))
        
        approval_line_ids = []
        approval_line_ids.append([0,0,{
                        'sts':'2',
                        'pelaksana_id':self._uid,
                        'tanggal':datetime.today(),
                        'division':self.division,
                    }])

        self.write({'approval_state':'am', 'state':'approved_it_section', 'approval_ids':approval_line_ids})
        self.send_mail_change_req_approval_it()
        return True


    def wkf_approval_it(self):
        user_groups = self.env['res.users'].browse()['groups_id']

        approval_line_ids = []
        approval_line_ids.append([0,0,{
                        'sts':'2',
                        'pelaksana_id':self._uid,
                        'tanggal':datetime.today(),
                        'division':self.division,
                    }])

        self.write({'approval_state':'a', 'state':'approved', 'approval_ids':approval_line_ids})
        return True
    
    # @api.multi
    def has_approved(self):
        if self.approval_state == 'a':
            return True
        return False
    
    # @api.multi
    def has_rejected(self):
        if self.approval_state == 'r':
            self.write({'state':'draft'})
            return True
        return False
    
    # @api.cr_uid_ids_context
    def wkf_set_to_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'draft','approval_state':'r'})
    
    # @api.cr_uid_ids_context
    def wkf_set_to_draft_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'draft','approval_state':'b'})

    def send_mail_change_req_approval(self):
        context = {}

        user = self.env['res.users'].search([('id','=',self._uid)])
        manager_empl = self.env['hr.employee'].search([('id','=',self.manager.id)])
        ir_action_id = self.env['ir.actions.actions'].search([('name','ilike','%Change Request Form%'),('type','=','ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name','ilike','%Change Request%'),('parent_id','=',False)])
        url = 'https://dess.daya-motor.com/web#id=%s&action=%s&model=dym.change.request&view_type=form&cids=&menu_id=%s' %(self.id,ir_action_id.id,ir_ui_menu_id.id)

        template = False
        if not template:
            template = self.env.ref('dym_change_request.send_cr_approval_notif')
        assert template._name == 'mail.template'

        context['url'] = url if url != False else ''

        if manager_empl != []:
            if not manager_empl.work_email:
                raise UserError(_("Cannot send email: user %s has no email address.") % manager_empl.name)
            with self.env.cr.savepoint():
                force_send = not(self.env.context.get('import_file', False))
                template.with_context(lang=user.lang, context=context, email_to=manager_empl.work_email).send_mail(user.id, force_send=True, raise_exception=True)


    def send_mail_change_req_approval_it(self,recipent=None):
        context = {}
        user = self.env['res.users'].search([('id','=',self._uid)])
        manager_empl = self.env['hr.employee'].search([('id','=',self.it_head.id)])
        ir_action_id = self.env['ir.actions.actions'].search([('name','ilike','%Change Request Form%'),('type','=','ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name','ilike','%Change Request%'),('parent_id','=',False)])
        url = 'https://dess.daya-motor.com/web#id=%s&action=%s&model=dym.change.request&view_type=form&cids=&menu_id=%s' %(self.id,ir_action_id.id,ir_ui_menu_id.id)

        template = False
        if not template:
            template = self.env.ref('dym_change_request.send_cr_approval_notif')
        assert template._name == 'mail.template'

        context['url'] = url if url != False else ''

        if manager_empl != []:
            if not manager_empl.work_email:
                raise UserError(_("Cannot send email: user %s has no email address.") % manager_empl.name)
            with self.env.cr.savepoint():
                email_to = manager_empl.work_email
                if recipent:
                    email_to = recipent
                force_send = not(self.env.context.get('import_file', False))
                template.with_context(lang=user.lang, context=context, email_to=email_to).send_mail(user.id, force_send=force_send, raise_exception=True)

    # sinta
    def send_mail_change_req_assign(self,developer_id,cr_id):
        company_id = self.env['res.company'].search([('id','=',1)])
        _logger.info(company_id)
        _logger.info('------------------------------------------')
        context = {}
        user = self.env['res.users'].search([('id','=',self._uid)])
        ir_action_id = self.env['ir.actions.actions'].search([('name','ilike','%Change Request Form%'),('type','=','ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name','ilike','%Change Request%'),('parent_id','=',False)])
        url = 'https://dess.daya-motor.com/web#id=%s&action=%s&model=dym.change.request&view_type=form&cids=&menu_id=%s' %(cr_id,ir_action_id.id,ir_ui_menu_id.id)
        template = False
        if not template:
            template = self.env.ref('dym_change_request.assign_cr_notif')
        assert template._name == 'mail.template'

        context['url'] = url if url != False else ''
        if developer_id :
            dev = self.env['hr.employee'].search([('id','=',developer_id)])
            if dev:
                if not dev.work_email:
                    raise UserError(_("Cannot send email: user %s has no email address.") % manager_empl.name)
                with self.env.cr.savepoint():
                    force_send = not(self.env.context.get('import_file', False))
                    template.with_context(lang=user.lang, context=context, email_to=dev.work_email,developer=dev.name,company_id=company_id
                                          ).send_mail(user.id, force_send=force_send, raise_exception=True)


    # Tambahan Egi
    def start_stop_dev(self):
        obj_ui_view = self.env["ir.ui.view"]
        obj_ir_view = obj_ui_view.search([("name", "=", 'dym.change.request.dev.form'),("model", "=", 'dym.change.request.dev'),])
        view_id = obj_ui_view.browse(obj_ir_view)
        change_request_browse = self.browse(self._ids[0])
        s_dev = self.env["dym.change.request.dev"].search([('change_request_id','=',change_request_browse.id)])
        return {
            'name':_("Start / Stop Development"),
            'view_mode': 'form',
            'view_id': view_id.id,
            'view_type': 'form',
            'res_model': 'dym.change.request.dev',
            'res_id': s_dev.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'change_request_id': change_request_browse.id,
                # 'developer_id': change_request_browse.developer_id.id,
            }
        }

    def start_stop_uat(self):
        obj_ui_view = self.env["ir.ui.view"]
        obj_ir_view = obj_ui_view.search([("name", "=", 'dym.change.request.uat.form'),("model", "=", 'dym.change.request.uat'),])
        view_id = obj_ui_view.browse(obj_ir_view)
        change_request_browse = self.browse(self._ids[0])
        s_uat = self.env["dym.change.request.uat"].search([('change_request_id','=',change_request_browse.id)])
        return {
            'name':_("Start / Stop UAT"),
            'view_mode': 'form',
            'view_id': view_id.id,
            'view_type': 'form',
            'res_model': 'dym.change.request.uat',
            'res_id': s_uat.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'change_request_id': change_request_browse.id,
                # 'developer_ids': change_request_browse.developer_ids.id,
                # 'user_id': change_request_browse.user_id.id,
            }
        }

    def wkf_uat_approval_it_analyst(self):
        # VALIDASI
        if self.it_analyst.id != self.env.user.employee_id.id and self.env.user.id != 2:
            raise Warning("This Approval Belongs to {name}".format(
                name=self.it_analyst.name
            ))

        approval_line_ids = []
        approval_line_ids.append([0,0,{
                        'sts':'2',
                        'reason':"Approve UAT to production",
                        'pelaksana_id':self._uid,
                        'tanggal':datetime.today(),
                        'division':self.division,
                    }])

        self.write({'state_process':'to_production', 'approval_ids':approval_line_ids}) 


    def live_in_production(self):
        self.write({'state':'done','state_process':'finish'}) 

    def cancel(self):
        self.write({'state':'cancel'}) 

    def refuse(self):
        self.write({'state':'refuse'}) 

class dym_change_request_line(models.Model):
    _name = "dym.change.request.line"

    cr_id = fields.Many2one('dym.change.request','Change Request Form')
    transaction_id = fields.Integer('Transaction ID')
    form_id = fields.Many2one('ir.model','Form')
    group_id = fields.Many2one('res.groups','Group', select=True)
    branch_id = fields.Many2one('dym.branch','Branch',select=True)
    division = fields.Selection([('Unit','Showroom'),('Sparepart','Workshop'),('Umum','General'),('Finance','Finance')], 'Division', change_default=True, index=True)
    limit = fields.Float('Limit', digits=(12,2))
    sts = fields.Selection([('1','Belum Approve'),('2','Approved'),('3','Revised'),('4','Cancelled')],'Status')
    reason = fields.Text('Reason')
    pelaksana_id = fields.Many2one('res.users','Pelaksana', size=128)
    tanggal = fields.Datetime('Tanggal Approve')

class dym_sequence_custome(models.Model):
    _inherit = "ir.sequence"

    def get_kode_divisi(self, cr, uid, division, context=None):
        if division == 'Unit':
            return '-S'
        elif division == 'Sparepart':
            return '-W'
        elif division == 'Finance':
            return '-F'
        elif division == False:
            return ''
        else:
            return '-G'

    def get_sequence(self, cr, uid, first_prefix, padding=3, context=None):
        ids = self.search([('name','=',first_prefix),('padding','=',padding)])
        if not ids:
            prefix = first_prefix + '.%(month)s%(y)s.'
            ids = self.create({'name': first_prefix,
                                 'implementation': 'no_gap',
                                 'prefix': prefix,
                                 'padding': padding})
            
        return self.get_id(ids.id)

# Tambahan Egi
class dym_change_request_developer_line(models.Model):
    _name = "dym.change.request.developer.line"

    cr_id = fields.Many2one('dym.change.request','Change Request Form')
    developer_id = fields.Many2one('hr.employee','Developer')

class dym_change_request_user_line(models.Model):
    _name = "dym.change.request.user.line"

    cr_id = fields.Many2one('dym.change.request','Change Request Form')
    user_id = fields.Many2one('hr.employee','User')