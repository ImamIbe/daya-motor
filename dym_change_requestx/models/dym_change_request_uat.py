import time
from datetime import datetime
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID
import logging
import time

class dym_change_req_uat(models.Model):
    _name = 'dym.change.request.uat'

    STATE_UAT_SELECTION = [
        ('in_progress','In Progress'),
        ('break','Break'),
        ('finish','Finish'),
    ]

    def _get_change_request_id(self):
        return self._context.get('change_request_id', False)

    # def _get_developer_ids(self):
    #     cr_id = self._context.get('change_request_id', False)
    #     obj_cr_b = self.env['dym.change.request']
    #     obj_id_b = obj_cr_b.search([('id','=',cr_id)])
    #     # _logger = logging.getLogger(__name__)
    #     # _logger.info('Developer Ids'+str(obj_id_b.developer_ids))
    #     return self._context.get('developer_ids', False)

    change_request_id = fields.Many2one('dym.change.request', string="Change Request", default=_get_change_request_id)
    change_request_uat_time_lines = fields.One2many('dym.change.request.uat.time.line','cr_uat_id',string="UAT Timeline")
    change_request_uat_notes = fields.One2many('dym.change.request.uat.note','cr_uat_id',string="Note Perbaikan")
    developer_ids = fields.One2many('dym.change.request.uat.developer.line', 'cr_uat_id', string="Developer")
    user_ids = fields.One2many('dym.change.request.uat.user.line', 'cr_uat_id', string="User")
    state_uat = fields.Selection(STATE_UAT_SELECTION, readonly=True)
    start = fields.Datetime('Start',readonly=True)
    finish = fields.Datetime('Finish',readonly=True)
    date_break = fields.Datetime('Break',readonly=True)
    end_break  = fields.Datetime('End Break',readonly=True)

    def btn_start(self):
        # _logger = logging.getLogger(__name__)
        tgl_start = time.strftime('%Y-%m-%d %H:%M:%S')
        a = self.browse(self._ids)
        obj_cr_a = self.env['dym.change.request']
        obj_id_a = obj_cr_a.search([('id','=',a.change_request_id.id)])
        obj_strt = obj_cr_a.browse(obj_id_a)
        inv = self.browse(self._ids[0])

        # _logger.info('Change Request ID '+str(obj_id_a))
        # _logger.info('Change Request Dev ID '+str(inv.id))
        
        if obj_strt :
            obj_id_a.write({'state_uat':'in_progress','start_uat':tgl_start,'developer_ids':inv.developer_ids.ids,'user_ids':inv.user_ids.ids})

            self.write({'state_uat':'in_progress','start':tgl_start})

            obj_cr_uat_time_line = self.env['dym.change.request.uat.time.line']
            time_line_values = {
                'cr_uat_id': inv.id,
                'state_time_line': 'start',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_uat_time_line.create(time_line_values)
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.uat',
            'res_id': self.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def btn_break(self):    
        # _logger = logging.getLogger(__name__)    
        tgl_break = time.strftime('%Y-%m-%d %H:%M:%S')
        b = self.browse(self._ids)
        obj_cr_b = self.env['dym.change.request']
        obj_id_b = obj_cr_b.search([('id','=',b.change_request_id.id)])
        obj_brk = obj_cr_b.browse(obj_id_b)
        cek_finish = b.change_request_id.finish_uat

        # _logger.info('CEK Finish '+str(cek_finish))

        obj_cr_uat_id = self.env['dym.change.request.uat']
        obj_uat_id = obj_cr_uat_id.search([('change_request_id','=',b.change_request_id.id),('start','!=',False)])

        # _logger.info('DILUAR')
        
        if obj_brk and not cek_finish :
            # _logger.info('DIDALAM')
            obj_id_b.write({'state_uat':'break','date_break_uat':tgl_break})
            self.write({'state_uat':'break','date_break':tgl_break,'start':b.change_request_id.start_uat,'end_break':b.change_request_id.end_break_uat,'finish':b.change_request_id.finish_uat})

            obj_uat_time_line = self.env['dym.change.request.uat.time.line']
            time_line_values = {
                'cr_uat_id': obj_uat_id.id,
                'state_time_line': 'break',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_uat_time_line.create(time_line_values) 
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.uat',
            'res_id': obj_uat_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    
    def btn_end_break(self):
        tgl_end_break = time.strftime('%Y-%m-%d %H:%M:%S')
        c = self.browse(self._ids)
        obj_cr_c = self.env['dym.change.request']
        obj_id_c = obj_cr_c.search([('id','=',c.change_request_id.id)])
        obj_ebrk = obj_cr_c.browse(obj_id_c)
        cek_finish2 = c.change_request_id.finish_uat

        obj_cr_uat_id = self.env['dym.change.request.uat']
        obj_uat_id = obj_cr_uat_id.search([('change_request_id','=',c.change_request_id.id),('start','!=',False)])
         
        if obj_ebrk and not cek_finish2 :
            obj_id_c.write({'state_uat':'in_progress','end_break_uat':tgl_end_break})
            self.write({'state_uat':'in_progress','end_break':tgl_end_break,'start':c.change_request_id.start_uat,'date_break':c.change_request_id.date_break_uat,'finish':c.change_request_id.finish_uat})

            obj_cr_time_line = self.env['dym.change.request.uat.time.line']
            time_line_values = {
                'cr_uat_id': obj_uat_id.id,
                'state_time_line': 'end_break',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_time_line.create(time_line_values)
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.uat',
            'res_id': obj_uat_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def btn_finish(self):
        tgl_finish = time.strftime('%Y-%m-%d %H:%M:%S')
        d = self.browse(self._ids)
        obj_cr_d = self.env['dym.change.request']
        obj_id_d = obj_cr_d.search([('id','=',d.change_request_id.id)])
        obj_fns = obj_cr_d.browse(obj_id_d)
        
        obj_cr_uat_id = self.env['dym.change.request.uat']
        obj_uat_id = obj_cr_uat_id.search([('change_request_id','=',d.change_request_id.id),('start','!=',False)])
        
        if obj_fns :
            # obj_id_d.write({'state_uat':'finish','finish_uat':tgl_finish, 'state_process' :'to_production'})
            state_process = 'wfa_it_analyst'
            if obj_id_d.create_date.date() < datetime.strptime('2022-03-09', '%Y-%m-%d').date():
                state_process = 'to_production'
            obj_id_d.write({'state_uat':'finish','finish_uat':tgl_finish, 'state_process' :state_process})

            obj_uat_id.write({'state_uat':'finish','finish':tgl_finish})

            obj_cr_uat_time_line = self.env['dym.change.request.uat.time.line']
            time_line_values = {
                'cr_uat_id': obj_uat_id.id,
                'state_time_line': 'finish',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_uat_time_line.create(time_line_values)  
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.uat',
            'res_id': obj_uat_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    # @api.onchange('change_request_id')
    # def _onchange_change_request_id(self):
    #     _logger = logging.getLogger(__name__)
    #     v = {}
    #     _logger.info('Change Request ID '+str(self.change_request_id))
    #     if self.change_request_id :
    #         _logger.info('Ada Change Request ID '+str(self.change_request_id))
    #         s = self.env["dym.change.request"].search([('id','=',self.change_request_id.id)])
    #         s_uat = self.env["dym.change.request.uat"].search([('change_request_id','=',self.change_request_id.id)])
    #         s_uat_time_line = self.env["dym.change.request.uat.time.line"].search([('cr_uat_id','=',s_uat.id)])
    #         s_uat_note = self.env["dym.change.request.uat.note"].search([('cr_uat_id','=',s_uat.id)])
    #         self.start = s.start
    #         self.finish = s.finish
    #         self.state_uat = s.state_uat  
    #         self.developer_id = s.developer_id
    #         self.user_id = s.user_id
    #         items = [] 
    #         _logger.info('Change Request UAT Timeline '+str(s_uat_time_line))
    #         if s_uat_time_line != []:
    #             for x in s_uat_time_line:
    #                 _logger.info('ISI '+str(x))
    #                 items.append([0,0,{
    #                     'state_time_line' : x.state_time_line,
    #                     'time' : x.time,
    #                 }])
    #             # print("Items receivable_scp_id"+str(items))
    #             self.change_request_uat_time_lines = items
    #         # self.change_request_dev_time_lines = s_dev_time_line.id
    #         items_uat = []
    #         _logger.info('Change Request UAT Note '+str(s_uat_note))
    #         if s_uat_note != []:
    #             for x in s_uat_note:
    #                 _logger.info('ISI '+str(x))
    #                 items_uat.append([0,0,{
    #                     'note' : x.note,
    #                 }])
    #             # print("Items receivable_scp_id"+str(items))
    #             self.change_request_uat_notes = items_uat                  
    #     else :
    #         _logger.info('Tidak Change Request ID ')
    #         self.start = False
    #         self.finish = False
    #         self.state_uat = False
        

class dym_change_req_uat_timeline(models.Model):
    _name = 'dym.change.request.uat.time.line'

    STATE_DEV_SELECTION = [
        ('start','Start'),
        ('break','Break'),
        ('end_break','End Break'),
        ('finish','Finish'),
    ]

    cr_uat_id = fields.Many2one('dym.change.request.uat', string="Change Request UAT")
    state_time_line = fields.Selection(STATE_DEV_SELECTION, 'State', readonly=True)
    time = fields.Datetime('Time', readonly=True)  

class dym_change_req_uat_note(models.Model):
    _name = 'dym.change.request.uat.note'

    def _get_change_request_uat_id(self):
        # _logger = logging.getLogger(__name__)
        cr_id = self._context.get('cr_id', False)
        # _logger.info('CR ID '+str(cr_id))
        cr_uat_id = self.env["dym.change.request.uat"].search([('change_request_id','=',cr_id)])
        # _logger.info('CR UAT ID '+str(cr_uat_id.id))
        return cr_uat_id.id

    cr_uat_id = fields.Many2one('dym.change.request.uat', string="Change Request UAT", default=_get_change_request_uat_id)
    note = fields.Text('Perbaikan')  

    # @api.model
    # def default_get(self, default_fields):
    #     rec = super(dym_change_req_uat_note, self).default_get(default_fields)
    #     _logger = logging.getLogger(__name__)

    #     active_ids = self.env.context.get('active_model')or self._context.get('active_id')
    #     active_model = self.env.context.get('active_model')

    #     _logger.info('Disini')
    #     _logger.info('Active Models '+str(active_model))

    #     if not active_ids or active_model != 'dym.change.request':
    #         _logger.info('Disini Ternyata')
    #         return rec

    #     change_request_id = self.env['dym.change.request'].browse(active_ids)
    #     change_request_uat_id = self.env["dym.change.request.uat"].search([('change_request_id','=',change_request_id.id)])

    #     _logger.info('Change Request ID UAT Note '+str(change_request_id))

    #     if change_request_uat_id:
    #         rec.update({
    #             'cr_uat_id': change_request_uat_id.id,
    #         })

    #     return rec

class dym_change_request_uat_developer_line(models.Model):
    _name = "dym.change.request.uat.developer.line"

    cr_uat_id = fields.Many2one('dym.change.request.uat','Change Request Form')
    developer_id = fields.Many2one('hr.employee','Developer')

class dym_change_request_uat_user_line(models.Model):
    _name = "dym.change.request.uat.user.line"

    cr_uat_id = fields.Many2one('dym.change.request.uat','Change Request Form')
    user_id = fields.Many2one('hr.employee','User')