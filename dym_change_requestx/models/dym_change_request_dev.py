import time
from datetime import datetime
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp import SUPERUSER_ID
import logging
import time
import logging
_logger = logging.getLogger(__name__)

class dym_change_req_dev(models.Model):
    _name = 'dym.change.request.dev'

    STATE_DEV_SELECTION = [
        ('in_progress','In Progress'),
        ('break','Break'),
        ('finish','Finish'),
    ]

    @api.depends('developer_ids.developer_id')
    def _get_task_order(self):
        for x in self:
            if x.developer_ids:
                _logger.info('llllllllll???????????????????????????????????') 
                cr_obj = self.env['dym.change.request']
                dev = [y.developer_id.id for y in x.developer_ids]

                if dev:
                    cr_search = self.env['dym.change.request'].search([('developer_dev_ids.developer_id','in',dev),('state','not in',['done','refuse','cancel'])])
                    if cr_search:
                        for c in cr_search:
                            cr_obj |= c
                    x.task_order_ids = cr_obj
            else:
                x.task_order_ids = False

    def _get_change_request_id(self):
        return self._context.get('change_request_id', False)

    change_request_id = fields.Many2one('dym.change.request', string="Change Request", default=_get_change_request_id)
    change_request_dev_time_lines = fields.One2many('dym.change.request.dev.time.line','cr_dev_id',string="Development Timeline")
    developer_ids = fields.One2many('dym.change.request.dev.developer.line', 'cr_dev_id', string="Developer")
    state_dev = fields.Selection(STATE_DEV_SELECTION, readonly=True)
    start = fields.Datetime('Start',readonly=True)
    finish = fields.Datetime('Finish',readonly=True)
    date_break = fields.Datetime('Break',readonly=True)
    end_break  = fields.Datetime('End Break',readonly=True)

    task_order_ids = fields.Many2many('dym.change.request','Task Order',compute=_get_task_order)

    def btn_start(self):
        # _logger = logging.getLogger(__name__)
        tgl_start = time.strftime('%Y-%m-%d %H:%M:%S')
        a = self.browse(self._ids)
        obj_cr_a = self.env['dym.change.request']
        obj_id_a = obj_cr_a.search([('id','=',a.change_request_id.id)])
        obj_strt = obj_cr_a.browse(obj_id_a)
        inv = self.browse(self._ids[0])

        # _logger.info('Change Request ID '+str(obj_id_a))
        # _logger.info('Change Request Dev ID '+str(inv.id))
        
        if obj_strt :
            obj_id_a.write({'state_process':'dev','state_dev':'in_progress','start_dev':tgl_start,'developer_ids':inv.developer_ids.ids})

            self.write({'state_dev':'in_progress','start':tgl_start})

            obj_cr_dev_time_line = self.env['dym.change.request.dev.time.line']
            time_line_values = {
                'cr_dev_id': inv.id,
                'state_time_line': 'start',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_dev_time_line.create(time_line_values)
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.dev',
            'res_id': self.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def btn_break(self):        
        tgl_break = time.strftime('%Y-%m-%d %H:%M:%S')
        b = self.browse(self._ids)
        obj_cr_b = self.env['dym.change.request']
        obj_id_b = obj_cr_b.search([('id','=',b.change_request_id.id)])
        obj_brk = obj_cr_b.browse(obj_id_b)
        cek_finish = b.change_request_id.finish_dev

        obj_cr_dev_id = self.env['dym.change.request.dev']
        obj_dev_id = obj_cr_dev_id.search([('change_request_id','=',b.change_request_id.id),('start','!=',False)])
        
        if obj_brk and not cek_finish :
            obj_id_b.write({'state_dev':'break','date_break_dev':tgl_break})
            self.write({'state_dev':'break','date_break':tgl_break,'start':b.change_request_id.start_dev,'end_break':b.change_request_id.end_break_dev,'finish':b.change_request_id.finish_dev})

            obj_dev_time_line = self.env['dym.change.request.dev.time.line']
            time_line_values = {
                'cr_dev_id': obj_dev_id.id,
                'state_time_line': 'break',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_dev_time_line.create(time_line_values) 
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.dev',
            'res_id': obj_dev_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    
    def btn_end_break(self):
        tgl_end_break = time.strftime('%Y-%m-%d %H:%M:%S')
        c = self.browse(self._ids)
        obj_cr_c = self.env['dym.change.request']
        obj_id_c = obj_cr_c.search([('id','=',c.change_request_id.id)])
        obj_ebrk = obj_cr_c.browse(obj_id_c)
        cek_finish2 = c.change_request_id.finish_dev

        obj_cr_dev_id = self.env['dym.change.request.dev']
        obj_dev_id = obj_cr_dev_id.search([('change_request_id','=',c.change_request_id.id),('start','!=',False)])
         
        if obj_ebrk and not cek_finish2 :
            obj_id_c.write({'state_dev':'in_progress','end_break_dev':tgl_end_break})
            self.write({'state_dev':'in_progress','end_break':tgl_end_break,'start':c.change_request_id.start_dev,'date_break':c.change_request_id.date_break_dev,'finish':c.change_request_id.finish_dev})

            obj_cr_time_line = self.env['dym.change.request.dev.time.line']
            time_line_values = {
                'cr_dev_id': obj_dev_id.id,
                'state_time_line': 'end_break',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_time_line.create(time_line_values)
        # return True
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.dev',
            'res_id': obj_dev_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def btn_finish(self):
        tgl_finish = time.strftime('%Y-%m-%d %H:%M:%S')
        d = self.browse(self._ids)
        obj_cr_d = self.env['dym.change.request']
        obj_id_d = obj_cr_d.search([('id','=',d.change_request_id.id)])
        obj_fns = obj_cr_d.browse(obj_id_d)
        
        obj_cr_dev_id = self.env['dym.change.request.dev']
        obj_dev_id = obj_cr_dev_id.search([('change_request_id','=',d.change_request_id.id),('start','!=',False)])
        
        if obj_fns :
            obj_id_d.write({'state_dev':'finish','finish_dev':tgl_finish, 'state_process' :'uat'})

            obj_dev_id.write({'state_dev':'finish','finish':tgl_finish})

            obj_cr_dev_time_line = self.env['dym.change.request.dev.time.line']
            time_line_values = {
                'cr_dev_id': obj_dev_id.id,
                'state_time_line': 'finish',
                'time': time.strftime('%Y-%m-%d %H:%M:%S'),
            }  
            create_time_line = obj_cr_dev_time_line.create(time_line_values)  
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'dym.change.request.dev',
            'res_id': obj_dev_id.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    # @api.onchange('change_request_id')
    # def _onchange_change_request_id(self):
    #     _logger = logging.getLogger(__name__)
    #     v = {}
    #     _logger.info('Change Request ID '+str(self.change_request_id))
    #     if self.change_request_id :
    #         _logger.info('Ada Change Request ID '+str(self.change_request_id))
    #         s = self.env["dym.change.request"].search([('id','=',self.change_request_id.id)])
    #         s_dev = self.env["dym.change.request.dev"].search([('change_request_id','=',self.change_request_id.id)])
    #         s_dev_time_line = self.env["dym.change.request.dev.time.line"].search([('cr_dev_id','=',s_dev.id)])
    #         self.start = s.start
    #         self.finish = s.finish
    #         self.state_dev = s.state_dev  
    #         self.developer_id = s.developer_id
    #         items = [] 
    #         _logger.info('Change Request Dev Timeline '+str(s_dev_time_line))
    #         if s_dev_time_line != []:
    #             for x in s_dev_time_line:
    #                 _logger.info('ISI '+str(x))
    #                 items.append([0,0,{
    #                     'state_time_line' : x.state_time_line,
    #                     'time' : x.time,
    #                 }])
    #             # print("Items receivable_scp_id"+str(items))
    #             self.change_request_dev_time_lines = items
    #         # self.change_request_dev_time_lines = s_dev_time_line.id                    
    #     else :
    #         _logger.info('Tidak Change Request ID ')
    #         self.start = False
    #         self.finish = False
    #         self.state_dev = False
        

class dym_change_req_dev_timeline(models.Model):
    _name = 'dym.change.request.dev.time.line'

    STATE_DEV_SELECTION = [
        ('start','Start'),
        ('break','Break'),
        ('end_break','End Break'),
        ('finish','Finish'),
    ]

    cr_dev_id = fields.Many2one('dym.change.request.dev', string="Change Request Development")
    state_time_line = fields.Selection(STATE_DEV_SELECTION, 'State', readonly=True)
    time = fields.Datetime('Time', readonly=True)  

# class hr_employee_dym_change_req_dev(models.Model):
#     _inherit = 'hr.employee'

#     cr_dev_id = fields.Many2one('dym.change.request.dev', string="Change Request Development")

class dym_change_request_dev_developer_line(models.Model):
    _name = "dym.change.request.dev.developer.line"

    cr_dev_id = fields.Many2one('dym.change.request.dev','Change Request Form')
    developer_id = fields.Many2one('hr.employee','Developer')

    # # sinta
    # @api.model
    # def create(self, values):
    #     developer_id = values.get('developer_id')
    #     cr_dev_id = values.get('cr_dev_id')
    #     cr = self.env['dym.change.request.dev'].search([('id','=',cr_dev_id)])
    #     cr_id = cr.change_request_id.id

    #     crfs = super(dym_change_request_dev_developer_line,self).create(values)
    #     if crfs :
    #         self.env['dym.change.request'].send_mail_change_req_assign(developer_id,cr_id)
    #     return crfs
