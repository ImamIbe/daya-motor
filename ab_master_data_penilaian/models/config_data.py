# -*- coding: utf-8 -*-

from odoo import models, fields, api, http, tools, _
from odoo.http import content_disposition, request, route, Controller
from odoo.exceptions import AccessError, ValidationError, MissingError, UserError
from odoo.tools.safe_eval import safe_eval, time
from odoo.addons.http_routing.models.ir_http import slugify
from odoo.tools import config, human_size, ustr, html_escape
from odoo.tools.mimetypes import guess_mimetype
from odoo.addons.web.controllers.main import ReportController

# from urllib3.exceptions import InsecureRequestWarning
# from urllib3 import disable_warnings

import logging
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import ssl
import requests
import json
ssl._create_default_https_context = ssl._create_unverified_context
import base64
import hashlib
import itertools
import mimetypes
import os
import re
from collections import defaultdict
import uuid
import base64
from pathlib import Path

import json
import werkzeug

# from bs4 import BeautifulSoup
# disable_warnings(InsecureRequestWarning)
# import urllib.request,urllib.parse,urllib.error




# List of content types that will be opened in browser




_logger = logging.getLogger(__name__)



class DeskripsiSubKriteria(models.Model):
    _name = 'deskripsi.sub.kriteria'
    _description = 'Deskripsi Sub Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Text('Name')
    kode = fields.Text('Kode')
    point = fields.Float('Point')
    sub_kriteria_id = fields.Many2one('sub.kriteria', string='Sub Kriteria')


class SubKriteria(models.Model):
    _name = 'sub.kriteria'
    _description = 'Sub Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Char('Name')
    kriteria_id = fields.Many2one('kriteria.kriteria', string='Kriteria')
    ket = fields.Text('Keterangan')


class KriteriaKriteria(models.Model):
    _name = 'kriteria.kriteria'
    _description = 'Kriteria'
    _sql_constraints = [(
        'name',
        'UNIQE (name)',
        'The name must be unique per company !'
    )]

    name = fields.Char('Name')


class PenilaianPenilaina(models.Model):
    _name = 'penilaian.penilaian'
    _description = 'Penilaian'

    kriteria_id = fields.Many2one('kriteria.kriteria', string='Kriteria')
    sub_kriteria_id = fields.Many2one('sub.kriteria', string='Sub Kriteria')
    des_sub_kriteria_id = fields.Many2one('deskripsi.sub.kriteria', string='Deskripsi Sub-Kriteria')
    kode = fields.Char('Kode')
    point = fields.Float('point')
    ket = fields.Text('Keterangan')

    penilaian_user_id = fields.Many2one('penilaian.user', string='Penilaian User', ondelete="cascade")

    @api.onchange('sub_kriteria_id')
    def _onchange_sub_kriteria_id(self):
        if self.sub_kriteria_id:
            self.kriteria_id = self.sub_kriteria_id.kriteria_id.id
            self.ket = self.sub_kriteria_id.ket
            # return {
            #     'domain':{
            #         'des_sub_kriteria_id': [('sub_kriteria_id','=', self.sub_kriteria_id.id)],
            #     }
            # }

    @api.onchange('des_sub_kriteria_id')
    def _onchange_des_sub_kriteria_id(self):
        if self.des_sub_kriteria_id:
            self.sub_kriteria_id = self.des_sub_kriteria_id.sub_kriteria_id.id
            self.kode = self.des_sub_kriteria_id.kode
            self.point = self.des_sub_kriteria_id.point
            self.ket = self.des_sub_kriteria_id.sub_kriteria_id.ket


class PenilaianUser(models.Model):
    _name = 'penilaian.user'
    _description = 'Penilaian User'

    def juri(self):
        group_obj = self.env['res.groups'].search([('name', 'in', ['ss Manager', 'ss Admin Komite'])])
        if group_obj:
            return [('id', 'in', group_obj.users.ids)]

    @api.model
    def default_get(self, field_list):
        res = super(PenilaianUser, self).default_get(field_list)
        group_obj = self.env['res.groups'].search([('name', 'in', ['ss Manager', 'ss Admin Komite'])])
        if group_obj:
            if self.env.uid in group_obj.users.ids:
                res_data = []
                sub_ktriteria = self.env['sub.kriteria'].search([])
                if sub_ktriteria:
                    for rec in sub_ktriteria:
                        sub_data = {
                            'kriteria_id': rec.kriteria_id.id,
                            'sub_kriteria_id': rec.id,
                            'ket': rec.ket,
                        }
                        res_data.append((0, 0, sub_data))
                res.update({
                    'user_id': self.env.uid,
                    'penilaian_ids': res_data,
                })
        return res

    user_id = fields.Many2one('res.users', string='User', domain=juri)
    total = fields.Float('Total', compute='_compute_total_user', store=True)
    penilaian_ids = fields.One2many('penilaian.penilaian', 'penilaian_user_id', string='Penilaian Kriteria',)

    sugestion_id = fields.Many2one('suggestion.system', string='Suggestion System', ondelete="cascade")

    @api.depends('penilaian_ids')
    def _compute_total_user(self):
        for rec in self:
            if len(rec.penilaian_ids) > 0:
                rec.total = sum([x.point for x in rec.penilaian_ids if rec.penilaian_ids])
            else:
                rec.total = 0


class SuggestionSystem(models.Model):
    _inherit = 'suggestion.system'
    _description = 'Suggestion System'

    penilaian_kriteria_ids = fields.One2many('penilaian.user', 'sugestion_id', string='Penilaian Kriteria')
    level = fields.Selection([
        ('bronze', 'Bronze'),
        ('silver', 'Silver'),
        ('gold', 'Gold'),
        ('platinum', 'Platinum'),
        ('diamond', 'Diamond')
    ], string='Level', compute='_compute_level', store=True)
    avarage = fields.Float('Avarage', compute='_compute_level', store=True)
    is_visible = fields.Boolean('Is Visible', default=False)

    @api.depends('penilaian_kriteria_ids')
    def _compute_level(self):
        for rec in self:
            jumlah = 0
            if len(rec.penilaian_kriteria_ids) > 0:
                jumlah = sum([x.total for x in rec.penilaian_kriteria_ids if rec.penilaian_kriteria_ids]) / len(rec.penilaian_kriteria_ids)
                rec.avarage = float(jumlah)

                if jumlah <= 50:
                    rec.level = 'bronze'
                elif jumlah > 50.1 and jumlah < 65:
                    rec.level = 'silver'
                elif jumlah >= 65.1 and jumlah < 82:
                    rec.level = 'gold'
                elif jumlah >= 82.1 and jumlah < 93:
                    rec.level = 'diamond'
                elif jumlah >= 93.1:
                    rec.level = 'diamond'

            else:
                rec.level = False
                rec.avarage = float(jumlah)

    def set_to_done_ss_admin_komite(self):
        for rec in self:
            rec.write({'state': 'done'})
        
    def kirim_data_ss(self):
        for record in self:
            return {
                'name': 'API Enlight',
                'type': 'ir.actions.act_window',
                'res_model': 'api.enlight',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': False,
                'views': [(False, 'form')],
                'target': 'new',
                'context': "{'default_suggestion_system_id':" + str(record.id) + "}",
            }
            # perbaikan = []
            # for data in record.perbaikan_lines:
            #     perbaikan.append(data.perbaikan)
            
            # kategory = ''
            # if record.dampak in ['cost', 'delivery']:
            #     kategory = 'Cost Reduction'
            # elif record.dampak == 'productivity':
            #     kategory = 'Productivity & Production System'
            # elif record.dampak in ['safety', 'morale', 'environment', 'others']:
            #     kategory = 'Safety, 5R & Environment'
            # elif record.dampak == 'quality':
            #     kategory = 'Quality'

            # pdf, _ = self.env.ref('ab_master_data_penilaian.action_report_suggestion')._render_qweb_pdf(self.id)        
            # pdfhttpheaders = [
            #     ('Content-Type', 'application/pdf'), 
            #     ('Content-Length', len(pdf)),
            #     ('Content-Disposition','attachement; filename=Suggestion System Form %s.pdf' % (self.name))]
            # response = request.make_response(pdf, headers=pdfhttpheaders)

            # url = 'https://enlightlibrary.triputra-group.com/api/api_enlight_send_other.php?action=uploadSS' # Set destination URL here
            # post_fields = {
            #     'FS_NIK': "1308131",
            #     # 'FS_NIK': str(rec.name),
            #     'FS_SUBCO':'Daya Adicipta Sandika',
            #     'FS_CAT': kategory,
            #     'FS_SBJ':'TEMA SS %s' % (record.tgl_ide),
            #     'FS_ABSTRAK':str(perbaikan),
            #     'FS_NAMATIM':'',
            #     'FS_ANGGOTA': str(record.employee_id.name),
            #     'key_API':'TR1pUTR@65844438rugbfsbjfjgdgfdjGGr',
            #     'file_contents': response,
            # }     # Set POST fields here
            # try:
            #     print("===============================1", post_fields)
            #     print("===============================2", urlencode(post_fields).encode())
            #     hasil = Request(url, urlencode(post_fields).encode())
            #     result = urlopen(hasil).read().decode()
            #     res = json.loads(result)
            #     if res['upload_theme']:
            #         for data in res['upload_theme']:
            #             if data['STATUS'] != 1 or data['STATUS'] != '1':
            #                 raise ValidationError(_(data['MSG']))
            # except UserError as e:
            #     raise e
            # except ValidationError as e:
            #     raise e
            # except (ValueError, KeyError, Exception) as e:
            #     raise UserError(_(e))
                


class ApiEnlight(models.TransientModel):
    _name = 'api.enlight'
    _description = 'Api Enlight'

    @api.model
    def _storage(self):
        return self.env['ir.config_parameter'].sudo().get_param('ir_attachment.location', 'file')

    @api.model
    def _filestore(self):
        return config.filestore(self._cr.dbname)

    @api.model
    def _file_write(self, bin_value, checksum):
        fname, full_path = self._get_path(bin_value, checksum)
        if not os.path.exists(full_path):
            try:
                with open(full_path, 'wb') as fp:
                    fp.write(bin_value)
                self._mark_for_gc(fname)
            except IOError:
                _logger.info("_file_write writing %s", full_path, exc_info=True)
        return fname
    
    @api.model
    def _file_read(self, fname):
        full_path = self._full_path(fname)
        try:
            with open(full_path, 'rb') as f:
                return f.read()
        except (IOError, OSError):
            _logger.info("_read_file reading %s", full_path, exc_info=True)
        return b''
    
    @api.model
    def _full_path(self, path):
        path = re.sub('[.]', '', path)
        path = path.strip('/\\')
        return os.path.join(self._filestore(), path)
    
    @api.model
    def _get_path(self, bin_data, sha):
        fname = sha[:3] + '/' + sha
        full_path = self._full_path(fname)
        if os.path.isfile(full_path):
            return fname, full_path 

        fname = sha[:2] + '/' + sha
        full_path = self._full_path(fname)
        dirname = os.path.dirname(full_path)
        if not os.path.isdir(dirname):
            os.makedirs(dirname)
        if os.path.isfile(full_path) and not self._same_content(bin_data, full_path):
            raise UserError("The attachment is colliding with an existing file.")
        return fname, full_path
    
    @api.model
    def _same_content(self, bin_data, filepath):
        BLOCK_SIZE = 1024
        with open(filepath, 'rb') as fd:
            i = 0
            while True:
                data = fd.read(BLOCK_SIZE)
                if data != bin_data[i * BLOCK_SIZE:(i + 1) * BLOCK_SIZE]:
                    return False
                if not data:
                    break
                i += 1
        return True

    @api.model
    def _file_delete(self, fname):
        self._mark_for_gc(fname)

    def _mark_for_gc(self, fname):
        """ Add ``fname`` in a checklist for the filestore garbage collection. """
        full_path = os.path.join(self._full_path('checklist'), fname)
        if not os.path.exists(full_path):
            dirname = os.path.dirname(full_path)
            if not os.path.isdir(dirname):
                with tools.ignore(OSError):
                    os.makedirs(dirname)
            open(full_path, 'ab').close()

    def _inverse_datas(self):
        self._set_attachment_data(lambda attach: base64.b64decode(attach.datas or b''))
    
    def _set_attachment_data(self, asbytes):
        for attach in self:
            bin_data = asbytes(attach)
            vals = self._get_datas_related_values(bin_data, attach.mimetype)

            fname = attach.store_fname
            super(ApiEnlight, attach.sudo()).write(vals)
            if fname:
                self._file_delete(fname)
    
    def _get_datas_related_values(self, data, mimetype):
        values = {
            'file_size': len(data),
            'checksum': self._compute_checksum(data),
            'index_content': self._index(data, mimetype),
            'store_fname': False,
            'db_datas': data,
        }
        if data and self._storage() != 'db':
            values['store_fname'] = self._file_write(data, values['checksum'])
            values['db_datas'] = False
        return values

    def _compute_checksum(self, bin_data):
        return hashlib.sha1(bin_data or b'').hexdigest()

    @api.depends('store_fname', 'db_datas', 'file_size')
    @api.depends_context('bin_size')
    def _compute_datas(self):
        if self._context.get('bin_size'):
            for attach in self:
                attach.datas = human_size(attach.file_size)
            return

        for attach in self:
            attach.datas = base64.b64encode(attach.raw or b'')
    
    @api.model
    def _index(self, bin_data, file_type):
        index_content = False
        if file_type:
            index_content = file_type.split('/')[0]
            if index_content == 'text': # compute index_content only for text type
                words = re.findall(b"[\x20-\x7E]{4,}", bin_data)
                index_content = b"\n".join(words).decode('ascii')
        return index_content
    
    def _inverse_raw(self):
        self._set_attachment_data(lambda a: a.raw or b'')

    @api.depends('store_fname', 'db_datas')
    def _compute_raw(self):
        for attach in self:
            if attach.store_fname:
                attach.raw = attach._file_read(attach.store_fname)
            else:
                attach.raw = attach.db_datas

    suggestion_system_id = fields.Many2one('suggestion.system', string='Suggestion System')

    file_name = fields.Char("File Name")

    raw = fields.Binary(string="File Content (raw)", compute='_compute_raw', inverse='_inverse_raw')
    datas = fields.Binary(string='File Content (base64)', compute='_compute_datas', inverse='_inverse_datas')
    db_datas = fields.Binary('Database Data', attachment=False)
    store_fname = fields.Char('Stored Filename')
    file_size = fields.Integer('File Size', readonly=True)
    checksum = fields.Char("Checksum/SHA1", size=40, index=True, readonly=True)
    mimetype = fields.Char('Mime Type', readonly=True)
    index_content = fields.Text('Indexed Content', readonly=True, prefetch=False)

    def kirim_data_ss(self):
        for rec in self:
            if rec.suggestion_system_id.company_id:
                if not rec.suggestion_system_id.company_id.user_name or rec.suggestion_system_id.company_id.user_name == False:
                    raise ValidationError(_('"User Name" in company must be made !'))

            perbaikan = []
            for data in rec.suggestion_system_id.perbaikan_lines:
                perbaikan.append(data.perbaikan)
            
            kategory = ''
            if rec.suggestion_system_id.dampak in ['cost', 'delivery']:
                kategory = 'Cost Reduction'
            elif rec.suggestion_system_id.dampak == 'productivity':
                kategory = 'Productivity & Production System'
            elif rec.suggestion_system_id.dampak in ['safety', 'morale', 'environment', 'others']:
                kategory = 'Safety, 5R & Environment'
            elif rec.suggestion_system_id.dampak == 'quality':
                kategory = 'Quality'
            
            full_path = self._full_path(rec.store_fname)
            print("full_path", full_path)
            
            path = "/home/yastaqiim/Downloads/python/file_testing.pdf"
            real_path = os.path.realpath(full_path)

            url = 'https://enlightlibrary.triputra-group.com/api/api_enlight_send_other.php?action=uploadSS' # Set destination URL here
            post_fields = {
                'FS_NIK': rec.suggestion_system_id.company_id.user_name,
                'FS_SUBCO':'Daya Adicipta Sandika',
                'FS_CAT': kategory,
                'FS_SBJ':'TEMA SS %s' % (rec.suggestion_system_id.tgl_ide),
                'FS_ABSTRAK':str(perbaikan),
                'FS_NAMATIM':'',
                'FS_ANGGOTA': str(rec.suggestion_system_id.employee_id.name),
                'key_API':'TR1pUTR@65844438rugbfsbjfjgdgfdjGGr',
            }     # Set POST fields here

            filename = 'file_testing.pdf'
            file = {'file_contents':(filename, open(real_path, 'rb'), 'multipart/form-data')}


            try:
                hasil = requests.post(url, data=post_fields, files=file, verify=False)
                if json.loads(hasil.text)['upload_theme']:
                    for data in json.loads(hasil.text)['upload_theme']:
                        if data['STATUS'] == 1 or data['STATUS'] == '1':
                            message = (_("%s") % data['MSG'])
                            return {
                                'effect': {
                                    'fadeout': 'slow',
                                    'message': message,
                                    'img_url': '/web/image/%s/%s/image_1024' % (self.env.user._name, self.env.user.user_id.id) if self.env.user.image_1024 else '/web/static/src/img/smile.svg',
                                    'type': 'rainbow_man',
                                    }
                            }
                        else:
                            raise ValidationError(_(data['MSG']))
            except UserError as e:
                raise e
            except ValidationError as e:
                raise e
            except (ValueError, KeyError, Exception) as e:
                raise UserError(_(e))