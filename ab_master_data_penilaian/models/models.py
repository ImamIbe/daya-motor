# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError

manager_group = 'ss Manager'


class SuggestionSystem(models.Model):
    _inherit = 'suggestion.system'
    _description = 'Suggestion System'

    company_id = fields.Many2one('res.company', string='Company')
    # company_id = fields.Many2one('res.company', string='Company', default= lambda self: self.env.company.search([('name','=', 'PT Daya Adicipta Sandika')],limit=1))
    is_visible = fields.Boolean('Is Visible', default=False)

    @api.onchange('employee_id')
    def _onchange_employee_id_to_company(self):
        if self.employee_id:
            self.company_id = self.employee_id.company_id.id    

    def compute_hasil_cost(self):
        res = super(SuggestionSystem, self).compute_hasil_cost()
        for ss in self:
            ss.is_visible = True
        return res

    def wkf_request_approval(self):
        res = super(SuggestionSystem, self).wkf_request_approval()

        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        context = {}
        template = False
        if not template:
            template_id = self.env.ref('ab_master_data_penilaian.email_template_suggestion_system_rfa').id
            template = self.env['mail.template'].browse(template_id)

        context['url'] = url if url != False else ''
        template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)
        return res

    def wkf_approval_manager(self):
        res = super(SuggestionSystem, self).wkf_approval_manager()

        template_id = self.env.ref('ab_master_data_penilaian.email_template_manager_approve')
        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        groups = self.env['res.groups'].search([('name', '=', 'ss Admin Komite')])

        context = {}
        template = False
        emails = ""
        context['url'] = url if url != False else ''

        if groups:
            for i in groups:
                for j in i.users:
                    emails += j.login + ', '
            emails += self.employee_id.work_email

            template_id.email_to = emails
            template = self.env['mail.template'].browse(template_id.id)
            template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        return res

    def wkf_approval_cek(self):
        groups = self.env['res.groups'].search([('name', '=', 'ss Admin Komite')])
        context = {}

        template_id = self.env.ref('ab_master_data_penilaian.email_template_pic_approve')
        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        emails = ''
        context['url'] = url if url != False else ''

        if groups:
            for i in groups:
                for j in i.users:
                    emails += j.login + ', '

            emails += self.employee_id.work_email
            template_id.email_to = emails
            template = self.env['mail.template'].browse(template_id.id)
            template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)


    def wkf_approval(self):
        res = super(SuggestionSystem, self).wkf_approval()

        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        context = {}
        template = False
        emails = ''

        context['url'] = url if url != False else ''
        for x in self:
            emails += x.employee_id.parent_id.work_email + ', '

        emails += self.employee_id.work_email

        template_id = self.env.ref('ab_master_data_penilaian.email_template_pic_approve')
        template_id.email_to = emails
        template = self.env['mail.template'].browse(template_id.id)
        template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        return res

    def cancel(self):
        res = super(SuggestionSystem, self).cancel()

        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        context = {}
        template = False
        if not template:
            template_id = self.env.ref('ab_master_data_penilaian.email_template_cancel').id
            template = self.env['mail.template'].browse(template_id)

        context['url'] = url if url != False else ''
        template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        return res

    def refuse(self):
        res = super(SuggestionSystem, self).refuse()

        template_id = self.env.ref('ab_master_data_penilaian.email_template_refuse')
        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        context = {}
        template = False
        emails = ''

        context['url'] = url if url != False else ''
        if self.state == 'waiting_for_approval':
            if not template:
                emails += self.employee_id.work_email

                template_id.email_to = emails
                template = self.env['mail.template'].browse(template_id.id)
                template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        if self.state == 'approved_manager':
            for x in self:
                emails += x.employee_id.parent_id.work_email + ', '

            emails += self.employee_id.work_email
            template_id.email_to = emails
            template = self.env['mail.template'].browse(template_id.id)
            template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        return res

    def revise(self):
        res = super(SuggestionSystem, self).revise()

        template_id = self.env.ref('ab_master_data_penilaian.email_template_revise')
        ir_action_id = self.env['ir.actions.actions'].search([('name', 'ilike', '%Suggestion System Form%'), ('type', '=', 'ir.actions.act_window')])
        ir_ui_menu_id = self.env['ir.ui.menu'].search([('name', 'ilike', '%Suggestion System Form%')])
        url = 'http://apps.daya-toyota.com:8069/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)
        # url = 'http://103.31.38.216:8070/web#id=%s&action=%s&model=suggestion.system&view_type=form&cids=&menu_id=%s' % (self.id, ir_action_id.id, ir_ui_menu_id.id)

        context = {}
        template = False
        emails = ''

        context['url'] = url if url != False else ''
        if self.state == 'waiting_for_approval':
            if not template:
                emails += self.employee_id.work_email

                template_id.email_to = emails
                template = self.env['mail.template'].browse(template_id.id)
                template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        if self.state == 'approved_manager':
            for x in self:
                emails += x.employee_id.parent_id.work_email + ', '

            emails += self.employee_id.work_email

            template_id.email_to = emails
            template = self.env['mail.template'].browse(template_id.id)
            template.with_context(context=context).send_mail(self.id, force_send=True, raise_exception=True)

        return res

    def write(self, values):
        res = super(SuggestionSystem, self).write(values)

        for record in self:
            if not record.lingkup_ide:
                raise UserError(_("Field Lingkup Ide Wajib Diisi!"))
            if not record.masalah_lines:
                raise UserError(_("Field Uraian Masalah Wajib Diisi!"))
            if not record.dampak:
                raise UserError(_("Field Dampak Wajib Diisi Salah Satu!"))
            if not record.perbaikan_lines:
                raise UserError(_("Field Usulan Perbaikan Wajib Diisi!"))
            if not record.penyebab_lines:
                raise UserError(_("Field Penyebab Wajib Diisi!"))
            if not record.estimasibiaya_lines:
                raise UserError(_("Field Estimasi Biaya Wajib Diisi!"))

            group_obj = self.env['res.groups'].search([('name', '=', manager_group)])
            if self.env.user.id in group_obj.users.ids:
                if not record.hasil_cost_lines:
                    raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))
                if not record.hasil_benefit_lines:
                    raise UserError(_("Field Perhitungan Benefit Wajib Diisi!"))

            if not record.dampak_qcdsmpe_lines:
                raise UserError(_("Field Dampak QCDSMPE Wajib Diisi!"))

        return res


    @api.constrains('hasil_cost_lines')
    def _check_parent_id(self):
        for record in self:
            if not record.hasil_cost_lines or record.hasil_cost_lines == False:
                raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))

    @api.constrains('hasil_benefit_lines')
    def _check_parent_id(self):
        for record in self:
            if not record.hasil_benefit_lines or record.hasil_benefit_lines == False:
                raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))
