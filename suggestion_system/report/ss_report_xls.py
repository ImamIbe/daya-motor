from odoo import models, exceptions, _
import time
import logging 
from odoo.exceptions import MissingError, UserError, ValidationError

class SSReportXls(models.AbstractModel):
    _name = 'report.suggestion_system.report_ss'
    _inherit = 'report.report_xlsx.abstract'

    def get_data(self, state, start_date, end_date, department_ids):
        # _logger = logging.getLogger(__name__)

        where_start_date = " 1=1 "
        if start_date:
            where_start_date = " ss.tgl_ide >= '%s' " % str(start_date)

        where_end_date = " 1=1 "
        if end_date:
            where_end_date = " ss.tgl_ide <=  '%s' " % str(end_date)

        where_dept_id = " 1=1 "
        if department_ids:
            where_dept_id = "  dep.id in %s " % str(tuple(department_ids)).replace(',)', ')')
            # where_dept_id = "  dep.id = %s " % str(department_ids)
        
        where_state = " 1=1 "
        if state:
            if state != 'all' :
                where_state = "  ss.state = '%s' " % str(state)
        


        # _logger.info('Dept id '+str(where_dept_id))
        # _logger.info('Dept id 2 '+str(where_dept_id2))   

        report_overtime = {
            'type': 'Track',
            'title': '',
            'title_short': ''}

#         query = """
#             select dep.name as department, ss.name as ss_name, emp.name as emp_name, emp.barcode as nik, ss.tema_ss as tema, ss.tgl_ide as tgl_ide, ss.lingkup_ide as lingkup_ide, ss.dampak as dampak, ss.total_biaya_estimasi as total_biaya_estimasi, ss.total_cost as total_cost, ss.total_benefit as total_benefit, ss.total_ratio as total_ratio, ss.ratio_percentage as ratio_percentage from suggestion_system ss
# left join hr_department dep on ss.department_id = dep.id
# left join hr_employee emp on ss.employee_id = emp.id
# where """+ where_dept_id +""" and
# """+ where_start_date  +"""and """+ where_end_date +"""
# and ss.state = 'approved'
#         """                 


        query = """
        select 
            par.name as parent,
            dep.name as department, 
            ss.name as ss_name, 
            emp.name as emp_name, 
            emp.barcode as nik, 
            ss.tema_ss as tema, 
            ss.tgl_ide as tgl_ide, 
            ss.lingkup_ide as lingkup_ide, 
            ss.dampak as dampak, 
            date(ss.create_date) as tgl_dibuat,
            coalesce(ss.total_biaya_estimasi,0) as total_biaya_estimasi, 
            coalesce(ss.total_cost, 0) as total_cost, 
            coalesce(ss.total_benefit, 0) as total_benefit, 
            coalesce(ss.total_ratio, 0) as total_ratio, 
            coalesce(ss.ratio_percentage,'0') as ratio_percentage,
            ss.state as state,
            date(ssal.tanggal) as last_approval,
            upper(ss.level) as level
        from suggestion_system ss
        left join hr_department dep on ss.department_id = dep.id
        left join hr_department par on par.id = dep.parent_id
        left join hr_employee emp on ss.employee_id = emp.id
        left join (
		select distinct transaction_id, max(tanggal) as tanggal from suggestion_system_approval_line group by transaction_id
        ) ssal on ssal.transaction_id = ss.id
        where dep.name != 'CEO'
        --and dep.loc not ilike 'Area : %'
        and dep.active = True
        and """+ where_dept_id +""" 
        and """+ where_start_date  +""" and """+ where_end_date +""" 
        and """+where_state+"""
        """

        reports = [report_overtime]

        datas2 = []
        for report in reports:
            self._cr.execute(query)
            # print(query_start)
            all_lines = self._cr.dictfetchall()
            # _logger.debug('All Lines '+str(all_lines))
            items = []

            if all_lines:
                datas = map(
                    lambda x: {
                        'no': 0,
                        'parent': str(x['parent'].encode('ascii','ignore').decode('ascii')) if x['parent'] != None else '',
                        'department': str(x['department'].encode('ascii','ignore').decode('ascii')) if x['department'] != None else '',
                        'ss_name': str(x['ss_name'].encode('ascii','ignore').decode('ascii')) if x['ss_name'] != None else '',
                        'tgl_dibuat': str(x['tgl_dibuat']) if x['tgl_dibuat'] != None else '',
                        'emp_name': str(x['emp_name'].encode('ascii','ignore').decode('ascii')) if x['emp_name'] != None else '',
                        'nik': str(x['nik'].encode('ascii','ignore').decode('ascii')) if x['nik'] != None else '',
                        'tema': str(x['tema'].encode('ascii','ignore').decode('ascii')) if x['tema'] != None else '',
                        'tgl_ide': str(x['tgl_ide']) if x['tgl_ide'] != None else '',
                        'lingkup_ide': str(x['lingkup_ide'].encode('ascii','ignore').decode('ascii')) if x['lingkup_ide'] != None else '',
                        'dampak': str(x['dampak'].encode('ascii','ignore').decode('ascii')) if x['dampak'] != None else '',
                        'total_biaya_estimasi': int(x['total_biaya_estimasi']) if x['total_biaya_estimasi'] != None else '',
                        'total_cost': int(x['total_cost']) if x['total_cost'] != None else '',
                        'total_benefit': int(x['total_benefit']) if x['total_benefit'] != None else '',
                        'total_ratio': int(x['total_ratio']) if x['total_ratio'] != None else '',
                        'ratio_percentage': str(x['ratio_percentage'].encode('ascii','ignore').decode('ascii')) if x['ratio_percentage'] != None else '',
                        'state': str(x['state'].encode('ascii','ignore').decode('ascii')) if x['state'] != None else '',
                        'last_approval': str(x['last_approval']) if x['last_approval'] != None else '',
                        'level': str(x['level']) if x['level'] != None else '',

                        # 'tgl': str(x['tgl'].encode('ascii','ignore').decode('ascii')) if x['tgl'] != None else '',
                        # 'jml_absen_karyawan': int(x['jml_absen_karyawan']) if x['jml_absen_karyawan'] != None else '',
                        # 'jml_karyawan': int(x['jml_karyawan']) if x['jml_karyawan'] != None else '',
                        },
                    all_lines)
                # _logger.info('Report '+str(datas))
                for p in datas:
                    datas2.append(p)
                # _logger.info('Report '+str(datas2))
                # reports = filter(lambda x: datas, [{'datas': datas}])
            
                if datas2 != []:
                    return datas2

    def generate_xlsx_report(self, workbook, data, lines):
        # _logger = logging.getLogger(__name__)

        # _logger.info('DISINI ')
        # _logger.info('DISINA '+str(lines))
        # _logger.info('DISINE '+str(data.get('reports')))
        department_ids = data.get('department_ids')
        start_date = data.get('start_date')
        end_date = data.get('end_date')
        state = data.get('state')

        datas = self.get_data(state, start_date, end_date, department_ids)

        row = 0
        col = 0

        sheet = workbook.add_worksheet("Report Suggestion System")
        bold = workbook.add_format({'bold': True})
        cell_format_int = workbook.add_format({'align': 'center',
                                    'valign': 'vcenter'})
        cell_format_int.set_num_format(1) 

        cell_format_int_l = workbook.add_format({'align': 'right',
                                    'valign': 'vcenter'})
        cell_format_int.set_num_format(1) 

        cell_format_float = workbook.add_format({'align': 'right',
                                    'valign': 'vcenter'})
        cell_format_float.set_num_format('0.00')

        header_format = workbook.add_format({'bold': True,
                                    'align': 'center',
                                    'valign': 'vcenter',
                                    'fg_color': '#fff89e',
                                    'border': 1})
        center_format = workbook.add_format({'align': 'center',
                                    'valign': 'vcenter'})

        footer_format = workbook.add_format({'bold': True,
                                    'align': 'right',
                                    'valign': 'vcenter',
                                    'fg_color': '#fff89e'})

        # sheet.set_column('B:E', 30)        
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:H', 20)
        sheet.set_column('I:I', 17)
        sheet.set_column('J:N', 20)
        sheet.set_column('O:S', 20)

        sheet.set_row(row + 3, 30)
        
        # sheet.set_row(row + 11, 30)

        sheet.write(row, 0, 'Report Suggestion System', bold)
        sheet.write(row + 1, 0, 'Tanggal : %s - %s' %(start_date, end_date))
        sheet.write(row + 3, 0, 'No.', header_format)
        sheet.write(row + 3, 1, 'Parent', header_format)
        sheet.write(row + 3, 2, 'Department', header_format)
        sheet.write(row + 3, 3, 'Nama SS', header_format)
        sheet.write(row + 3, 4, 'Nama Karyawan', header_format)
        sheet.write(row + 3, 5, 'NIK', header_format)
        sheet.write(row + 3, 6, 'Tema', header_format)
        sheet.write(row + 3, 7, 'Tanggal Ide', header_format)
        sheet.write(row + 3, 8, 'Lingkup Ide', header_format)
        sheet.write(row + 3, 9, 'Dampak', header_format)
        sheet.write(row + 3, 10, 'Total Biaya Estimasi', header_format)
        sheet.write(row + 3, 11, 'Total Cost', header_format)
        sheet.write(row + 3, 12, 'Total Benefit', header_format)
        sheet.write(row + 3, 13, 'Total Ratio', header_format)
        sheet.write(row + 3, 14, 'Persentasi Ratio', header_format)
        sheet.write(row + 3, 15, 'Status', header_format)
        sheet.write(row + 3, 16, 'Approval Terakhir', header_format)
        sheet.write(row + 3, 17, 'Tanggal Dibuat', header_format)
        sheet.write(row + 3, 18, 'Level', header_format)
        


        row += 4
        no = 1

        if datas:
            for x in datas:
                # _logger.info('OBJ '+str(x))
                
                sheet.write(row, 0, no, center_format)
                sheet.write(row, 1, x['parent'])
                sheet.write(row, 2, x['department'])
                sheet.write(row, 3, x['ss_name'])
                sheet.write(row, 4, x['emp_name'])
                sheet.write(row, 5, x['nik'])
                sheet.write(row, 6, x['tema'])
                sheet.write(row, 7, x['tgl_ide'], center_format)
                sheet.write(row, 8, x['lingkup_ide'])
                sheet.write(row, 9, x['dampak'])
                sheet.write(row, 10, x['total_biaya_estimasi'], cell_format_int_l)
                sheet.write(row, 11, x['total_cost'], cell_format_int_l)
                sheet.write(row, 12, x['total_benefit'], cell_format_int_l)
                sheet.write(row, 13, x['total_ratio'], cell_format_int_l)
                sheet.write(row, 14, x['ratio_percentage'], cell_format_float)
                sheet.write(row, 15, x['state'].upper().replace('_', ' '))
                sheet.write(row, 16, x['last_approval'], center_format)
                sheet.write(row, 17, x['tgl_dibuat'], center_format)
                sheet.write(row, 18, x['level'], center_format)


                row += 1
                no += 1

            sum_total_biaya = '=SUM(K5:K'+str(row)+')'
            sum_total_cost = '=SUM(L5:L'+str(row)+')'
            sum_total_benefit = '=SUM(M5:M'+str(row)+')'
            sum_total_ratio = '=SUM(N5:N'+str(row)+')'
            

            for i in range(0,9):
                sheet.write(row,i, None, footer_format)

            sheet.write(row,9, sum_total_biaya, footer_format)
            sheet.write(row,10, sum_total_cost, footer_format)
            sheet.write(row,11, sum_total_benefit, footer_format)
            sheet.write(row,12, sum_total_ratio, footer_format)
            sheet.write(row,13, None, footer_format)
            sheet.write(row,14, None, footer_format)
            sheet.write(row,15, None, footer_format)
            sheet.write(row,16, None, footer_format)
            sheet.write(row,17, None, footer_format)
            sheet.write(row,18, None, footer_format)
            

            sheet.write(row + 1, 0, None)
            sheet.write(row + 2, 0, time.strftime('%Y-%m-%d %H:%M:%S') + ' ' + str(self.env['res.users'].browse(self._uid).name))
        else:
            sheet.write(row, 1, 'No Data Found')
            sheet.write(row + 3, 0, time.strftime('%Y-%m-%d %H:%M:%S') + ' ' + str(self.env['res.users'].browse(self._uid).name))