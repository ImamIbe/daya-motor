from odoo import models, api,fields
from odoo.osv import osv

# Wizard Confirm Button
class wizard_revise_ss(models.TransientModel):
    _name = "wizard.revise.ss"

    ss_id = fields.Many2one('suggestion.system', 'SS')
    text =  fields.Text()

    # @api.multi
    def yes_oric(self):
        # return True
        # self.env['dym.stock.packing'].post()
        # self.env['dym.stock.packing'].search([('id','=',self.ss_id.id)]).post()
        self.env['suggestion.system'].search([('id','=',self.ss_id.id)]).with_context(okey=True,text=self.text).revise()
        
wizard_revise_ss()