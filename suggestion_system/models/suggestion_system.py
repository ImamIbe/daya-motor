import time
from datetime import datetime

from lxml import etree

from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError
from openerp import SUPERUSER_ID

from odoo.osv import osv
import logging

manager_group = 'ss Manager'


def get_unique_numbers(numbers):
    list_of_unique_numbers = []
    unique_numbers = set(numbers)

    for number in unique_numbers:
        list_of_unique_numbers.append(number)
    return list_of_unique_numbers


class suggestion_system(models.Model):
    _name = 'suggestion.system'
    _description = "Suggestion System Form"

    STATE_SELECTION = [
        ('draft', 'Draft'),
        ('waiting_for_approval', 'Waiting Approval'),
        ('approved_manager', 'Approved Manager'),
        ('approved', 'PIC SS Approved'),
        ('cancel', 'Cancelled'),
        ('refuse', 'Refused'),
        ('revise', 'Revised'),
        ('done', 'Done'),
    ]

    @api.onchange('employee_id')
    def _get_department(self):
        for ss in self:
            if ss.employee_id:
                employee = self.env['hr.employee'].search([('id', '=', ss.employee_id.id)])
                ss.department_id = employee.department_id.id
            else:
                ss.department_id = False

    def _get_default_employee_id(self):
        group_obj = self.env['res.groups'].search([('name', '=', manager_group)])
        if not (self.env.user.id in group_obj.users.ids):
            return self.env.user.employee_id

    name = fields.Char('SS Name')
    tema_ss = fields.Text('Tema Perbaikan', required=True)
    employee_id = fields.Many2one('hr.employee', 'Creator', required=True, default=_get_default_employee_id)
    # rel_nik = fields.Char(related='employee_id.barcode', string='NIK', readonly=True)
    department_id = fields.Many2one('hr.department', 'Department', required=True)

    tgl_ide = fields.Date(string='Tanggal Ide', default=datetime.today(), required=True)
    lokasi_ide = fields.Char('Lokasi Pelaksanaan Ide', required=True)
    lingkup_ide = fields.Selection([('pekerjaannya', 'Pekerjaannya'), ('bukan_pekerjaannya', 'Bukan Pekerjaannya')], 'Lingkup Ide', required=True)

    # page kiri
    # safety = fields.Boolean('Safety / 5 R')
    # cost = fields.Boolean('Cost')
    # productivity = fields.Boolean('Productivity')
    # environment = fields.Boolean('Environment')
    # quality = fields.Boolean('Quality')
    # delivery = fields.Boolean('Delivery')
    # morale = fields.Boolean('Morale')
    # others = fields.Boolean('Others')

    dampak = fields.Selection([('safety', 'Safety / 5R'), ('cost', 'Cost'), ('productivity', 'Productivity'), ('environment', 'Environment'),
                              ('quality', 'Quality'), ('delivery', 'Delivery'), ('morale', 'Morale'), ('others', 'Others')], string='Dampak', required=True)

    masalah_lines = fields.One2many('suggestion.system.masalah.line', 'ss_id', string="Masalah", required=True)
    perbaikan_lines = fields.One2many('suggestion.system.perbaikan.line', 'ss_id', string="Perbaikan", required=True)
    penyebab_lines = fields.One2many('suggestion.system.penyebab.line', 'ss_id', string="Penyebab")
    estimasibiaya_lines = fields.One2many('suggestion.system.estimasibiaya.line', 'ss_id', string='Estimasi Biaya')
    total_biaya_estimasi = fields.Integer(string='Total Estimasi', compute='_compute_total_biaya_estimasi', store=True)

    # page tengah
    keadaan_lines = fields.One2many('suggestion.system.keadaan.line', 'ss_id', string='Keadaan Sebelum dan Sesudah', required=True)
    hasil_lines = fields.One2many('suggestion.system.hasil.line', 'ss_id', string="Hasil & Dampak Perbaikan")
    hasil_cost_lines = fields.One2many('suggestion.system.hasil.cost.line', 'ss_id', string='Hasil Perhitungan Cost')

    hasil_benefit_lines = fields.One2many('suggestion.system.benefit.line', 'ss_id', string='Hasil Perhitungan Benefit')
    # hasil_ratio_lines = fields.One2many('suggestion.system.ratio.line', 'ss_id', string='Hasil Perhitungan Ratio Cost/Benefit', readonly=True)

    total_cost = fields.Integer('Total Cost')
    total_benefit = fields.Integer('Total Benefit')
    total_ratio = fields.Float('Total Ratio')
    # ratio_percentage = fields.Float('Ratio Percentage')
    ratio_percentage = fields.Char('Ratio Percentage')

    dampak_qcdsmpe_lines = fields.One2many('suggestion.system.dampak.line', 'ss_id', string='Dampak QCDSMPE')

    # state and page kanan
    state = fields.Selection(STATE_SELECTION, string='State', readonly=True, default='draft')
    state_cancel = fields.Selection(related='state')
    state_refuse = fields.Selection(related='state')
    state_revise = fields.Selection(related='state')
    ss_approval_lines = fields.One2many('suggestion.system.approval.line', 'ss_id', string="Approval")
    approval_ids = fields.One2many('suggestion.system.approval.line', 'transaction_id', string="Table Approval")
    approval_state = fields.Selection([
        ('b', 'Belum Request'),
        ('rf', 'Request For Approval'),
        ('am', 'Approved Manager'),
        ('a', 'Approved'),
        ('r', 'Reject'),
        ('rv', 'Revise'),
        ('c', 'Cancel'),
    ], 'Approval State', readonly=True, default='b')

    # syahril 2021-08-16
    nik_excel = fields.Char(string='NIK (excel)')

    # PIC IMPROVEMENT
    level = fields.Selection([
        ('bronze', 'Bronze'),
        ('silver', 'Silver'),
        ('gold', 'Gold'),
        ('platinum', 'Platinum'),
        ('diamond', 'Diamond')
    ], string='Level')

    hasil_cost_lines_pic_imp = fields.One2many('suggestion.system.hasil.cost.line.pic_imp', 'ss_id', string='Hasil Perhitungan Cost')
    hasil_benefit_lines_pic_imp = fields.One2many('suggestion.system.benefit.line.pic_imp', 'ss_id', string='Hasil Perhitungan Benefit')

    total_cost_pic_imp = fields.Integer(compute='_compute_hasil_cost_benefit_pic_imp', string='Total Cost', store=True)
    total_benefit_pic_imp = fields.Integer(compute='_compute_hasil_cost_benefit_pic_imp', string='Total Benefit', store=True)
    total_ratio_pic_imp = fields.Float(compute='_compute_hasil_cost_benefit_pic_imp', string='Total Ratio', store=True)
    ratio_percentage_pic_imp = fields.Char(compute='_compute_hasil_cost_benefit_pic_imp', string='Ratio Percentage', store=True)

    @api.constrains('lingkup_ide', 'masalah_lines', 'dampak', 'perbaikan_lines', 'penyebab_lines', 'estimasibiaya_lines', 'keadaan_lines', 'hasil_lines', 'dampak_qcdsmpe_lines')
    def cek_data(self):
        for record in self:
            if not record.lingkup_ide:
                # raise osv.except_osv(_('Warning!'),_("Field Lingkup Ide Wajib Diisi!"))
                raise UserError(_("Field Lingkup Ide Wajib Diisi!"))
            if not record.masalah_lines:
                # raise osv.except_osv(_('Warning!'),_("Field Uraian Masalah Wajib Diisi!"))
                raise UserError(_("Field Uraian Masalah Wajib Diisi!"))
            if not record.dampak:
                # raise osv.except_osv(_('Warning!'),_("Field Dampak Wajib Diisi Salah Satu!"))
                raise UserError(_("Field Dampak Wajib Diisi Salah Satu!"))
            if not record.perbaikan_lines:
                # raise osv.except_osv(_('Warning!'),_("Field Usulan Perbaikan Wajib Diisi!"))
                raise UserError(_("Field Usulan Perbaikan Wajib Diisi!"))
            if not record.penyebab_lines:
                # raise osv.except_osv(_('Warning!'),_("Field Penyebab Wajib Diisi!"))
                raise UserError(_("Field Penyebab Wajib Diisi!"))
            if not record.estimasibiaya_lines:
                # raise osv.except_osv(_('Warning!'),_("Field Estimasi Biaya Wajib Diisi!"))
                raise UserError(_("Field Estimasi Biaya Wajib Diisi!"))

            group_obj = self.env['res.groups'].search([('name', '=', manager_group)])
            if self.env.user.id in group_obj.users.ids:
                if not record.hasil_cost_lines:
                    # raise osv.except_osv(_('Warning!'),_("Field Perhitungan Cost Wajib Diisi!"))
                    raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))
                if not record.hasil_benefit_lines:
                    # raise osv.except_osv(_('Warning!'),_("Field Perhitungan Benefit Wajib Diisi!"))
                    raise UserError(_("Field Perhitungan Benefit Wajib Diisi!"))

            if not record.dampak_qcdsmpe_lines:
                # raise osv.except_osv(_('Warning!'),_("Field Dampak QCDSMPE Wajib Diisi!"))
                raise UserError(_("Field Dampak QCDSMPE Wajib Diisi!"))

    # PIC IMPROVEMENT
    @api.depends('hasil_cost_lines_pic_imp', 'hasil_benefit_lines_pic_imp')
    def _compute_hasil_cost_benefit_pic_imp(self):
        for record in self:
            record.total_ratio_pic_imp = 0
            record.ratio_percentage_pic_imp = '0'

            if record.hasil_cost_lines_pic_imp:
                total = 0
                for x in record.hasil_cost_lines_pic_imp:
                    total += x.biaya
                record.total_cost_pic_imp = total
            else:
                record.total_cost_pic_imp = 0

            if record.hasil_benefit_lines_pic_imp:
                total = 0
                for x in record.hasil_benefit_lines_pic_imp:
                    total += x.benefit
                record.total_benefit_pic_imp = total
            else:
                record.total_benefit_pic_imp = 0

            record.total_ratio_pic_imp = record.total_benefit_pic_imp - record.total_cost_pic_imp
            if record.total_cost_pic_imp > 0:
                ratio_percentage_pic_imp = record.total_benefit_pic_imp / record.total_cost_pic_imp * 100
                record.ratio_percentage_pic_imp = str(round(ratio_percentage_pic_imp, 2))+"%"
            else:
                record.ratio_percentage_pic_imp = 'INFINITY'

    @api.model
    def create(self, values):
        employee_id = False
        if values.get('nik_excel'):
            nik = values['nik_excel']
            emp_obj = self.env['hr.employee'].search([('barcode', '=', nik)])
            if emp_obj:
                values['employee_id'] = emp_obj[0].id
                values['department_id'] = emp_obj[0].department_id.id
            else:
                # raise osv.except_osv(_('Warning!'), _("NIK Tidak Ada ! (nik : %s)" % str(nik)))
                raise UserError(_('Warning!'), _("NIK Tidak Ada ! (nik : %s)" % str(nik)))

        if values['employee_id']:
            employee_id = values['employee_id']
            employee_id = self.env['hr.employee'].search([('id', '=', employee_id)])

        # values['name'] = self.env['ir.sequence'].get_sequence(self._cr, self._uid, 'SS')
        print("===============================zzzz", values['company_id'])
        company_obj = self.env['res.company']
        company = company_obj.browse(values['company_id'])
        values['name'] = company_obj.get_sequence(company)

        # # page kiri
        if not values.get('lingkup_ide'):
            # raise osv.except_osv(_('Warning!'), _("Field Lingkup Ide Wajib Diisi!"))
            raise UserError(_("Field Lingkup Ide Wajib Diisi!"))
        if not values.get('masalah_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Uraian Masalah Wajib Diisi!"))
            raise UserError(_("Field Uraian Masalah Wajib Diisi!"))
        if not values.get('dampak'):
            # raise osv.except_osv(_('Warning!'), _("Field Dampak Wajib Diisi Salah Satu!"))
            raise UserError(_("Field Dampak Wajib Diisi Salah Satu!"))
        if not values.get('perbaikan_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Usulan Perbaikan Wajib Diisi!"))
            raise UserError(_("Field Usulan Perbaikan Wajib Diisi!"))
        if not values.get('penyebab_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Penyebab Wajib Diisi!"))
            raise UserError(_("Field Penyebab Wajib Diisi!"))
        if not values.get('estimasibiaya_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Estimasi Biaya Wajib Diisi!"))
            raise UserError(_("Field Estimasi Biaya Wajib Diisi!"))

        # # Page tengah
        if not values.get('keadaan_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Keadaan Sebelum dan Sesudah Wajib Diisi!"))
            raise UserError(_("Field Keadaan Sebelum dan Sesudah Wajib Diisi!"))
        if not values.get('hasil_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Evaluasi Hasil Wajib Diisi!"))
            raise UserError(_("Field Evaluasi Hasil Wajib Diisi!"))

        # # Mandatory Only for ss Manager
        group_obj = self.env['res.groups'].search([('name', '=', manager_group)])
        if self.env.user.id in group_obj.users.ids:
            if not values.get('hasil_cost_lines'):
                # raise osv.except_osv(_('Warning!'), _("Field Perhitungan Cost Wajib Diisi!"))
                raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))
            if not values.get('hasil_benefit_lines'):
                # raise osv.except_osv(_('Warning!'), _("Field Perhitungan Benefit Wajib Diisi!"))
                raise UserError(_("Field Perhitungan Benefit Wajib Diisi!"))

        if not values.get('dampak_qcdsmpe_lines'):
            # raise osv.except_osv(_('Warning!'), _("Field Dampak QCDSMPE Wajib Diisi!"))
            raise UserError(_("Field Dampak QCDSMPE Wajib Diisi!"))

        # Dampak Lama
        # if (not values.get('safety')) and (not values.get('cost')) and (not values.get('productivity')) and (not values.get('environment')) and (not values.get('quality')) and (not values.get('delivery')) and (not values.get('morale')) and (not values.get('others')):
        #     raise osv.except_osv(_('Warning!'),_("Field Dampak Wajib Diisi Minimal Salah Satunya!"))

        # # Jika estimasi biaya diisi tapi perhitungan cost tidak diisi
        # if values.get('estimasibiaya_lines'):
        #     if not values.get('hasil_cost_lines'):
        #         raise osv.except_osv(_('Warning!'),_("Field Perhitungan Cost Wajib Diisi Jika Estimasi Biaya telah diisi!"))

        # page Tengah
        # if not values.get('keadaan_lines'):
        #     raise osv.except_osv(_('Warning!'),_("Estimasi Biaya belum diisi!"))
        # if not values.get('hasil_lines'):
        #     raise osv.except_osv(_('Warning!'),_("Estimasi Biaya belum diisi!"))

        # if values.get('hasil_cost_lines') and (not values.get('estimasibiaya_lines')):
        #     raise osv.except_osv(_('Warning!'),_("Estimasi Biaya belum diisi!"))

        if values.get('nik_excel'):
            res.compute_hasil_cost()

        res = super(suggestion_system, self).create(values)
        return res

    def write(self, values):
        # cek kalo semua data di masalah dihapus
        # Page Kiri
        if values.get('masalah_lines') != None:
            masalah_avail = []
            for masalah in values.get('masalah_lines'):
                hps_masalah = masalah[0]
                masalah_avail.append(hps_masalah)
            masalah_avail_unq = get_unique_numbers(masalah_avail)
            if (masalah_avail_unq[0] == 2) and (len(masalah_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Uraian Masalah Harus Diisi!"))
                raise UserError(_("Field Uraian Masalah Harus Diisi!"))

        if values.get('penyebab_lines') != None:
            penyebab_avail = []
            for penyebab in values.get('penyebab_lines'):
                hps_penyebab = penyebab[0]
                penyebab_avail.append(hps_penyebab)
            penyebab_avail_unq = get_unique_numbers(penyebab_avail)
            if (penyebab_avail_unq[0] == 2) and (len(penyebab_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Penyebab Wajib Diisi!"))
                raise UserError(_("Field Penyebab Wajib Diisi!"))

        if values.get('perbaikan_lines') != None:
            perbaikan_avail = []
            for perbaikan in values.get('perbaikan_lines'):
                hps_perbaikan = perbaikan[0]
                perbaikan_avail.append(hps_perbaikan)
            perbaikan_avail_unq = get_unique_numbers(perbaikan_avail)
            if (perbaikan_avail_unq[0] == 2) and (len(perbaikan_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Usulan Perbaikan Harus Diisi!"))
                raise UserError(_("Field Usulan Perbaikan Harus Diisi!"))

        if values.get('estimasibiaya_lines') != None:
            estimasibiaya_avail = []
            for estimasibiaya in values.get('estimasibiaya_lines'):
                hps_estimasibiaya = estimasibiaya[0]
                estimasibiaya_avail.append(hps_estimasibiaya)
            estimasibiaya_avail_unq = get_unique_numbers(estimasibiaya_avail)
            if (estimasibiaya_avail_unq[0] == 2) and (len(estimasibiaya_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Estimasi Biaya Wajib Diisi!"))
                raise UserError(_("Field Estimasi Biaya Wajib Diisi!"))

        # Page Tengah
        if values.get('keadaan_lines') != None:
            keadaan_avail = []
            for keadaan in values.get('keadaan_lines'):
                hps_keadaan = keadaan[0]
                keadaan_avail.append(hps_keadaan)
            keadaan_avail_unq = get_unique_numbers(keadaan_avail)
            if (keadaan_avail_unq[0] == 2) and (len(keadaan_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Keadaan Sebelum dan Sesudah Wajib Diisi!"))
                raise UserError(_("Field Keadaan Sebelum dan Sesudah Wajib Diisi!"))

        if values.get('hasil_lines') != None:
            hasil_avail = []
            for hasil in values.get('hasil_lines'):
                hps_hasil = hasil[0]
                hasil_avail.append(hps_hasil)
            hasil_avail_unq = get_unique_numbers(hasil_avail)
            if (hasil_avail_unq[0] == 2) and (len(hasil_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Evaluasi Hasil Wajib Diisi!"))
                raise UserError(_("Field Evaluasi Hasil Wajib Diisi!"))

        # Mandatory only for SS Manager
        group_obj = self.env['res.groups'].search([('name', '=', manager_group)])
        if self.env.user.id in group_obj.users.ids:
            if values.get('hasil_cost_lines') != None:
                hasil_cost_avail = []
                for hasil_cost in values.get('hasil_cost_lines'):
                    hps_hasil_cost = hasil_cost[0]
                    hasil_cost_avail.append(hps_hasil_cost)
                hasil_cost_avail_unq = get_unique_numbers(hasil_cost_avail)
                if (hasil_cost_avail_unq[0] == 2) and (len(hasil_cost_avail_unq) == 1):
                    # raise osv.except_osv(_('Warning!'),_("Field Perhitungan Cost Wajib Diisi!"))
                    raise UserError(_("Field Perhitungan Cost Wajib Diisi!"))
            else:
                hasil_cost_lines = self.hasil_cost_lines
                i = 0
                for data in hasil_cost_lines:
                    i = i + 1
                if i < 1:
                    # raise osv.except_osv(_('Warning!'),_('Field Perhitungan Cost Wajib Diisi!'))
                    raise UserError(_('Field Perhitungan Cost Wajib Diisi!'))

            if values.get('hasil_benefit_lines') != None:
                hasil_benefit_avail = []
                for hasil_benefit in values.get('hasil_benefit_lines'):
                    hps_hasil_benefit = hasil_benefit[0]
                    hasil_benefit_avail.append(hps_hasil_benefit)
                hasil_benefit_avail_unq = get_unique_numbers(hasil_benefit_avail)
                if (hasil_benefit_avail_unq[0] == 2) and (len(hasil_benefit_avail_unq) == 1):
                    # raise osv.except_osv(_('Warning!'),_("Field Perhitungan Benefit Wajib Diisi!"))
                    raise UserError(_("Field Perhitungan Benefit Wajib Diisi!"))
            else:
                hasil_benefit_lines = self.hasil_benefit_lines
                i = 0
                for data in hasil_benefit_lines:
                    i = i + 1
                if i < 1:
                    # raise osv.except_osv(_('Warning!'),_('Field Perhitungan Benefit Wajib Diisi!'))
                    raise UserError(_('Field Perhitungan Benefit Wajib Diisi!'))

        if values.get('dampak_qcdsmpe_lines') != None:
            dampak_qcdsmpe_avail = []
            for dampak_qcdsmpe in values.get('dampak_qcdsmpe_lines'):
                hps_dampak_qcdsmpe = dampak_qcdsmpe[0]
                dampak_qcdsmpe_avail.append(hps_dampak_qcdsmpe)
            dampak_qcdsmpe_avail_unq = get_unique_numbers(dampak_qcdsmpe_avail)
            if (dampak_qcdsmpe_avail_unq[0] == 2) and (len(dampak_qcdsmpe_avail_unq) == 1):
                # raise osv.except_osv(_('Warning!'),_("Field Dampak QCDSMPE Wajib Diisi!"))
                raise UserError(_("Field Dampak QCDSMPE Wajib Diisi!"))

        res = super(suggestion_system, self).write(values)
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        adm_user_group = 'ss PIC Improvement'
        res = super(suggestion_system, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        if view_type == 'form':
            doc = etree.XML(res['arch'])
            node = doc.xpath("//field[@name='employee_id']")[0]

            group_obj = self.env['res.groups'].search([('name', '=', adm_user_group)])
            if not (self.env.user.id in group_obj.users.ids):
                domain_filter = "[('user_id','=','%s')]" % str(self.env.user.id)
                node.set('domain', domain_filter)
                # node.set('readonly','True')
                res['arch'] = etree.tostring(doc)
        return res

    @api.depends('estimasibiaya_lines')
    def _compute_total_biaya_estimasi(self):
        total = 0
        for ss in self:
            if ss.estimasibiaya_lines:
                for x in ss.estimasibiaya_lines:
                    # raise osv.except_osv(_('Warning!'),_(str(x.ss_id)+':'+str(ss.id)+'.......'+str(x.deskripsi_biaya)+''))
                    total = total + x.biaya
                ss.total_biaya_estimasi = total
            else:
                ss.total_biaya_estimasi = 0

    def compute_hasil_cost(self):
        # raise osv.except_osv(_('Warning!'),_(str(self.hasil_cost_lines)+':'+str(self.estimasibiaya_lines)))
        for ss in self:
            ss.total_ratio = 0
            # ss.ratio_percentage = 0
            ss.ratio_percentage = '0'

            if not ss.hasil_cost_lines:
                # raise osv.except_osv(_('Warning!'),_("Perhitungan Cost Harus diisi terlebih dahulu"))
                ss.total_cost = 0

            if not ss.hasil_benefit_lines:
                ss.total_benefit = 0

            if ss.hasil_cost_lines and ss.hasil_benefit_lines:
                total_cost = 0
                for x in ss.hasil_cost_lines:
                    total_cost = total_cost + x.biaya
                total_benefit = 0
                for y in ss.hasil_benefit_lines:
                    total_benefit = total_benefit + y.benefit

                ss.total_cost = total_cost
                ss.total_benefit = total_benefit

                ss.total_ratio = ss.total_benefit - ss.total_cost

                if ss.total_cost > 0:
                    ratio_percentage = ss.total_benefit / ss.total_cost * 100
                    ss.ratio_percentage = str(round(ratio_percentage, 2))+"%"
                else:
                    ss.ratio_percentage = 'INFINITY'

    def wkf_request_approval(self):
        if self.hasil_cost_lines:
            # raise osv.except_osv(_('Warning!'),_("val: "+str(values['total_cost'])+' val:'+str(values['total_benefit'])))
            # if self.total_cost == 0 and self.total_benefit == 0:
            if self.ratio_percentage == '' and self.ratio_percentage == None:
                raise osv.except_osv(_('Warning!'), _("Klik Tombol Generate Terlebih dahulu Sebelum Request Approval "))

        self.compute_hasil_cost()

        if self.state == 'draft':
            self.write({'state': 'waiting_for_approval', 'approval_state': 'rf'})
        if self.state == 'revise':
            approve_by = ''
            for approval_id in self.approval_ids:
                if approval_id.sts == '2':
                    approve_by = approval_id.sts
                # raise osv.except_osv(_('Warning!'),_("val : "+str(approve_by)))
            if approve_by == '2':
                self.write({'state': 'approved_manager', 'approval_state': 'am'})
            else:
                self.write({'state': 'waiting_for_approval', 'approval_state': 'rf'})
        return True

    def wkf_approval_manager(self):
        self.compute_hasil_cost()
        if self.total_ratio < 0:
            # raise osv.except_osv(_('Warning!'),_("Proses Approve tidak dapat dilakukan karena Total NQI (Benefit) < 0! "))
            raise UserError(_("Proses Approve tidak dapat dilakukan karena Total NQI (Benefit) < 0! "))

        approval_line_ids = []
        approval_line_ids.append([0, 0, {
            'sts': '2',
            'pelaksana_id': self._uid,
            'tanggal': datetime.today(),
        }])

        self.write({'state': 'approved_manager', 'approval_state': 'am', 'approval_ids': approval_line_ids})
        return True

    def wkf_approval(self):
        approval_line_ids = []
        approval_line_ids.append([0, 0, {
            'sts': '5',
            'pelaksana_id': self._uid,
            'tanggal': datetime.today(),
        }])
        if not self.level:
            raise Warning("Silahkan Isi Level untuk SS ini terlebih dahulu!")

        self.write({'state': 'approved', 'approval_state': 'a', 'approval_ids': approval_line_ids})
        return True

    def cancel(self):
        approval_line_ids = []
        approval_line_ids.append([0, 0, {
            'sts': '4',
            'pelaksana_id': self._uid,
            'tanggal': datetime.today(),
        }])
        self.write({'state': 'cancel', 'approval_state': 'c', 'approval_ids': approval_line_ids})
        return True

    def refuse(self):
        approval_line_ids = []
        approval_line_ids.append([0, 0, {
            'sts': '3',
            'pelaksana_id': self._uid,
            'tanggal': datetime.today(),
        }])
        self.write({'state': 'refuse', 'approval_state': 'r', 'approval_ids': approval_line_ids})
        return True

    def revise(self):

        okey = self._context.get('okey', False)
        text = self._context.get('text', '')

        if not okey:
            context = {
                'default_ss_id': self.id
            }
            return{
                'type': 'ir.actions.act_window',
                'name': 'Revise Reason',
                'res_model': 'wizard.revise.ss',
                'view_type': 'form',
                # 'view_type':'ir.actions.act_window',
                'view_mode': 'form',
                'target': 'new',
                'context': context
            }

        approval_line_ids = []
        approval_line_ids.append([0, 0, {
            'sts': '6',
            'pelaksana_id': self._uid,
            'reason': text,
            'tanggal': datetime.today(),
        }])
        self.write({'state': 'revise', 'approval_state': 'rv', 'approval_ids': approval_line_ids})
        return True


# Usulan Perbaikan Page
class suggestion_system_masalah(models.Model):
    _name = 'suggestion.system.masalah.line'
    _description = "Suggestion System (Masalah)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    masalah = fields.Text('Masalah', required=True)


class suggestion_system_perbaikan(models.Model):
    _name = 'suggestion.system.perbaikan.line'
    _description = "Suggestion System (Perbaikan)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    perbaikan = fields.Text('Perbaikan', required=True)


class suggestion_system_penyebab(models.Model):
    _name = 'suggestion.system.penyebab.line'
    _description = "Suggestion System (Penyebab)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    penyebab = fields.Text('Penyebab', required=True)
    attachment = fields.One2many('suggestion.system.penyebab.attachment', 'penyebab_id', string="Attachment")


class suggestion_system_penyebab_attachment(models.Model):
    _name = 'suggestion.system.penyebab.attachment'
    _description = "Suggestion System (Penyebab Attachment)"

    penyebab_id = fields.Many2one('suggestion.system.penyebab.line', 'Suggestion System')
    attachment = fields.Binary('Attachment')
    attachment_name = fields.Text('Attachment Name')
    attachment_view = fields.Binary(related='attachment', string='Attachment view')


class suggestion_system_estimasiBiaya(models.Model):
    _name = 'suggestion.system.estimasibiaya.line'
    _description = 'Suggestion System (Estimasi Biaya)'

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    deskripsi_biaya = fields.Char('Deskripsi', required=True)
    biaya = fields.Integer('Biaya', required=True)


# Laporan Penerapan Page
class suggestion_system_keadaan(models.Model):
    _name = 'suggestion.system.keadaan.line'
    _description = 'Suggestion System (Keadaan sebelum dan sesudah)'

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    k_sebelum = fields.Char('Keadaan Sebelum', required=True)
    k_sesudah = fields.Char('Keadaan Sesudah', required=True)
    k_sebelum_attachment = fields.Binary('Keadaan Sebelum (Attachmnent)')
    k_sebelum_attachment_name = fields.Text('Keadaan Sebelum (Attachmnent Name)')
    k_sebelum_attachment_view = fields.Binary(related='k_sebelum_attachment', string='Attachment Sebelum (view)')

    k_sesudah_attachment = fields.Binary('Keadaan Sesudah (Attachmnent)')
    k_sesudah_attachment_name = fields.Text('Keadaan Sesudah (Attachmnent Name)')
    k_sesudah_attachment_view = fields.Binary(related='k_sesudah_attachment', string='Attachment Sesudah (view)')

    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    #     res = super(suggestion_system_keadaan, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     k_sebelum_attachment_name = self.k_sebelum_attachment_name
    #     k_sesudah_attachment_name = self.k_sesudah_attachment_name

    #     raise osv.except_osv(_('Warning!'),_("data: "+str(self.k_sebelum)+'...'+str(self.k_sebelum)+str(res['arch'])))

    #     if view_type == 'form':
    #         doc = etree.XML(res['arch'])
    #         node_sebelum = doc.xpath("//field[@name='k_sebelum_attachment_view']")[0]
    #         node_sesudah = doc.xpath("//field[@name='k_sesudah_attachment_view']")[0]

    #     if k_sebelum_attachment_name.endswith('.pdf'):
    #         widget_sebelum = "pdf_viewer"
    #     elif not k_sebelum_attachment_name.endswith('.pdf'):
    #         # widget = image_viewer
    #         widget_sebelum = "image"

    #     if k_sesudah_attachment_name.endswith('.pdf'):
    #         # widget = pdf_viewer
    #         widget_sesudah = "pdf_viewer"
    #     elif not k_sesudah_attachment_name.endswith('.pdf'):
    #         # widget = image_viewer
    #         widget_sesudah = "image"

    #     node_sebelum.set('widget',widget_sebelum)
    #     node_sesudah.set('widget',widget_sesudah)
    #     res['arch'] = etree.tostring(doc)

    #     return res


class suggestion_system_hasil(models.Model):
    _name = 'suggestion.system.hasil.line'
    _description = "Suggestion System (Hasil & Dampak Perbaikan)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    hasil = fields.Text('Hasil & Dampak Perbaikan', required=True)
    attachment = fields.One2many('suggestion.system.hasil.attachment', 'hasil_id', string="Attachment")


class suggestion_system_hasil_attachment(models.Model):
    _name = 'suggestion.system.hasil.attachment'
    _description = "Suggestion System (Hasil Attachment)"

    hasil_id = fields.Many2one('suggestion.system.hasil.line', 'Suggestion System')
    attachment = fields.Binary('Attachment')
    attachment_name = fields.Text('Attachment Name')
    attachment_view = fields.Binary(related='attachment', string='Attachment view')


class suggestion_system_hasil_cost(models.Model):
    _name = 'suggestion.system.hasil.cost.line'
    _description = "Suggestion System (Hasil Cost)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    deskripsi_biaya = fields.Char('Deskripsi', required=True)
    biaya = fields.Integer('Biaya', required=True)


class suggestion_system_benefit(models.Model):
    _name = 'suggestion.system.benefit.line'
    _description = "Suggestion System (benefit)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    deskripsi_biaya = fields.Char('Deskripsi', required=True)
    benefit = fields.Integer('benefit', required=True)


# PIC IMPROVEMENT
class suggestion_system_hasil_cost_pic_imp(models.Model):
    _name = 'suggestion.system.hasil.cost.line.pic_imp'
    _description = "Suggestion System (Hasil Cost PIC Improvement)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    deskripsi_biaya = fields.Char('Deskripsi', required=True)
    biaya = fields.Integer('Biaya', required=True)


class suggestion_system_benefit_pic_imp(models.Model):
    _name = 'suggestion.system.benefit.line.pic_imp'
    _description = "Suggestion System (benefit PIC Improvement)"

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    deskripsi_biaya = fields.Char('Deskripsi', required=True)
    benefit = fields.Integer('benefit', required=True)


class suggestion_system_dampak_qcdsmpe(models.Model):
    _name = 'suggestion.system.dampak.line'
    _description = 'Suggestion System (Dampak QCDSMPE)'

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System')
    dampak_qcdsmpe = fields.Char('Dampak QCDSMPE')


# Page Approval
class dym_change_request_line(models.Model):
    _name = "suggestion.system.approval.line"
    _description = 'Suggestion System (approval)'

    ss_id = fields.Many2one('suggestion.system', 'Suggestion System Form')
    transaction_id = fields.Integer('Transaction ID')
    form_id = fields.Many2one('ir.model', 'Form')
    group_id = fields.Many2one('res.groups', 'Group', select=True)
    limit = fields.Float('Limit', digits=(12, 2))
    sts = fields.Selection([('1', 'Belum Approve'), ('2', 'Approved Manager'), ('3', 'Refused'), ('4', 'Cancelled'), ('5', 'Approved'), ('6', 'Revised')], 'Status')
    reason = fields.Text('Reason')
    pelaksana_id = fields.Many2one('res.users', 'Pelaksana', size=128)
    tanggal = fields.Datetime('Tanggal Approve')


class dym_sequence(models.Model):
    _inherit = "ir.sequence"

    def get_sequence(self, cr, uid, first_prefix='SS', padding=3, context=None):
        ids = self.search([('name', '=', first_prefix), ('padding', '=', padding)])
        if not ids:
            prefix = first_prefix + '.%(month)s%(y)s.'
            ids = self.create({'name': first_prefix,
                               'implementation': 'no_gap',
                               'prefix': prefix,
                               'padding': padding})

        return self.get_id(ids.id)



class ResCompany(models.Model):
    _inherit = 'res.company'
    _description = 'Res Company'

    default_code = fields.Char('Default Code Sequence Company')
    user_name = fields.Char('User Name')

    def create_sequence(self, data):
        if data:
            if not data.default_code:
                raise UserError(('"Default Code Sequence Company" in company must be made !'))
            sequence = self.env['ir.sequence'].create({
                'name': '{}, {}'.format(data._name, data.name),
                'implementation': 'no_gap',
                'code': '{},{}'.format(data._name, data.id),
                'prefix': 'SS.{}.{}.'.format(data.default_code, '%(month)s%(y)s'),
                'padding': 3,
                'range_reset': 'monthly',
            })
            return sequence

        return 'recursive'

    def get_sequence(self, data):
        result = self.env['ir.sequence'].next_by_code('{},{}'.format(data._name, data.id))
        if not result:
            sequence_created = self.create_sequence(data)
            if sequence_created == 'recursive':
                raise UserError(('Choose the type of company!'))
            else:
                return self.get_sequence(data)
        return result
    
