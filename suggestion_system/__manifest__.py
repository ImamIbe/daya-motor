# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Suggestion System',
    'version': '1.0',
    'category': 'Employee Improvement',
    'sequence': 81,
    'summary': 'Suggestion System Form Online',
    'description': """
       """,
    # 'website': '',
    'depends': ['base','report_xlsx','hr','sequence_reset_period'],
    'images': [
        'static/description/icon.png'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/suggestion_system_group.xml',
	    'views/suggestion_system_custom_css.xml',
        'views/suggestion_system_view.xml',
        'views/suggestion_system_line_view.xml',
        'views/res_company.xml',
        'wizard/wizard.xml',
        'wizard/report_wizard.xml'
    ],
    'demo': [
        # 'data/hr_attendance_demo.xml'
        # 'views/suggestion_system_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'qweb': [
        # "static/src/xml/attendance.xml",
    ],
    'application': True,
}
