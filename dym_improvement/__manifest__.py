# -*- coding: utf-8 -*-

{
    'name': 'Improvement',
    'version': '1.1',
    'category': 'Improvement Daya Motor',
    'sequence': 75,
    'summary': 'Insert QCC, QCP, QCL, PPS, 5R, Safety Improvement for daya motor',
    'description': "",
    'depends': [
        'base', 'hr', 'suggestion_system'
    ],
    'images': [
        'static/description/icon.png'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/dym_improvement_view.xml',
        'views/dym_improvement_line_view.xml',
        'views/improvement_step_view.xml',
        'wizard/revise_wizard.xml',
        'data/mail_template.xml',
        'data/scheduled_action.xml',
        'data/send_mail_action.xml',
        'data/send_email_template.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
